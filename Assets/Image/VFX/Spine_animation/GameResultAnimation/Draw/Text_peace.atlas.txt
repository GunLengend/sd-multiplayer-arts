
Text_peace.png
size: 614,612
format: RGBA8888
filter: Linear,Linear
repeat: none
BG
  rotate: false
  xy: 2, 264
  size: 452, 346
  orig: 452, 346
  offset: 0, 0
  index: -1
HueSaturation 1
  rotate: true
  xy: 290, 17
  size: 245, 132
  orig: 245, 132
  offset: 0, 0
  index: -1
HueSaturation 1 copy 3
  rotate: true
  xy: 2, 51
  size: 211, 286
  orig: 211, 286
  offset: 0, 0
  index: -1
HueSaturation 1 copy 4
  rotate: true
  xy: 456, 264
  size: 346, 156
  orig: 346, 156
  offset: 0, 0
  index: -1
ef1
  rotate: false
  xy: 48, 14
  size: 35, 35
  orig: 35, 35
  offset: 0, 0
  index: -1
ef2
  rotate: true
  xy: 424, 201
  size: 61, 64
  orig: 61, 64
  offset: 0, 0
  index: -1
ef3
  rotate: false
  xy: 2, 4
  size: 44, 45
  orig: 44, 45
  offset: 0, 0
  index: -1
