﻿
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class ZMatchFinding : NetworkBehaviour
{
    private ZUser User;
    public List<RoomInfo> listRoomNormal;
    public List<RoomInfo> listRoomRanking;

    public ZGameRoom Room;

    #region FIND MATCH CLIENT
    void Start()
    {
        User = GetComponent<ZUser>();
    }

    public override void OnStartLocalPlayer()
    {
        base.OnStartLocalPlayer();

        this.RegisterListener(ZEventId.OnUserClickFindMatch, (sender, param) => CmdOnUserFindMatch((FindMatchInfo)param));
        this.RegisterListener(ZEventId.OnJoinRoomWithId, (sender, param) => CmdOnUserJoinMatch((JoinMatchInfo)param));
        this.RegisterListener(ZEventId.OnUserCreateRoom, (sender, param) => CmdOnUserCreateRoom((CreateMatchInfo)param));
    }

    [TargetRpc]
    public void TargetOnGameFindingMatch(NetworkConnection con)
    {
        //TODO: Turn off UI Select match and display the UI Finding match
        Debug.Log("ZMatchFinding: Match founded !");
    }

    [TargetRpc]
    public void TargetOnGameMatchFound(NetworkConnection con, ZGameRoom.RoomGame connectInfo, int maxConnection, string playerToken)
    {
        MatchInfo matchInfo = new MatchInfo
        {
            roomInfo = connectInfo,
            maxPlayerConnect = maxConnection,
            playerToken = playerToken
        };

        Debug.Log("ZMatchFinding: Client found match");

        //Send event Match Found
        this.PostEvent(ZEventId.OnGameMatchFound, matchInfo);
    }

    [TargetRpc]
    public void TargetReceiveRoomDoesntExist(NetworkConnection netCon)
    {
        this.PostEvent(ZEventId.OnRoomDoesNotExist);
        Debug.Log("ZMatchFinding: Room doesn't exist");
    }

    [TargetRpc]
    public void TargetReceiveRoomIsFull(NetworkConnection netCon)
    {
        this.PostEvent(ZEventId.OnRoomIsFull);
        Debug.Log("ZMatchFinding: Room is full");

        //TODO: Display popup UI
    }

    [TargetRpc]
    public void TargetReceiveNotEnoughCondition(NetworkConnection netCon, ZGameRoom.RoomType roomType)
    {
        this.PostEvent(ZEventId.OnUserNotHaveEnoughChip, roomType);
        Debug.Log("ZMatchFinding: You dont match condition to join room");
    }

    [TargetRpc]
    public void TargetReceiveCannotJoinedRoom(NetworkConnection netCon)
    {
        this.PostEvent(ZEventId.OnCannotJoinRoom);
        Debug.Log("ZMatchFinding: You cannot join this room");
    }

    [TargetRpc]
    public void TargetReceiveWrongRoomPassword(NetworkConnection netCon)
    {
        this.PostEvent(ZEventId.OnInputWrongPassword);
        Debug.Log("ZMatchFinding: You type wrong room password");
    }
    #endregion

    #region FIND MATCH SERVER
    [Command]
    public void CmdOnUserFindMatch(FindMatchInfo matchInfo)
    {
        //Check the user have enough money or not
        bool isOK = FindMatchCondition(User, matchInfo.roomType);

        //If the user has enough gold
        if (isOK)
        {
            //Add to matchmaking queue
            ZMatchMaking.Instance.AddPlayerToQueue(User, matchInfo);

            //Then told the the client open match finding loading screen
            TargetOnGameFindingMatch(this.connectionToClient);
        }
        else
        {
            //If not, told the user stop find match and display popup.
        }
    }

    [Command]
    public void CmdOnUserJoinMatch(JoinMatchInfo matchInfo)
    {
        //Find out the index of that roomId get from the client
        switch (matchInfo.roomType)
        {
            case ZGameRoom.RoomType.Normal:
                Room = ZMatchMaking.Instance.listRoomNormal.Find(x => x.roomID == matchInfo.roomId);
                break;
            case ZGameRoom.RoomType.Tournament:
                Room = ZMatchMaking.Instance.listRoomRanking.Find(x => x.roomID == matchInfo.roomId);
                break;
        }

        //If room id still equal -1 that mean this room does not exit
        if (Room == null)
        {
            //Send the result that room does not exist.
            TargetReceiveRoomDoesntExist(this.connectionToClient);

            //Then quit that process
            return;
        }
        else
        {
            //That mean room exist, then check is full or not
            if (Room.roomState == ZGameRoom.RoomState.Full)
            {
                //Send the result that room is full.
                TargetReceiveRoomIsFull(this.connectionToClient);

                //Then quit that process
                return;
            }
            else
            {
                //Room is not full, let check the user have enough money or not
                bool isOK = FindMatchCondition(User, matchInfo.roomType);

                //If the user has pass condition
                if (isOK)
                {
                    //Check the password correct or not
                    if (!string.Equals(matchInfo.roomPassword ?? string.Empty, Room.roomPassword ?? string.Empty))
                    {
                        Debug.Log("ZMatchFiding: input password : " + matchInfo.roomPassword + " room password : " + Room.roomPassword);

                        //If incorrect, send the wrong password to the user
                        TargetReceiveWrongRoomPassword(this.connectionToClient);
                    }
                    else
                    {
                        //Check the user inside have another "me" or not, it's happen when disconnect and relog into one room
                        bool alreadyExist = Room.allUser.Contains(User);

                        if (alreadyExist)
                            TargetReceiveCannotJoinedRoom(User.connectionToClient);
                        else
                            //If not exist "me", let the user join room
                            Room.OnPlayerJoinRoom(User);
                    }
                }
                else
                {
                    //If not, told the user stop create match and display popup.

                }
            }
        }
    }

    [Command]
    public void CmdOnUserCreateRoom(CreateMatchInfo matchInfo)
    {
        //Check the user has match condition or not
        bool isOK = FindMatchCondition(User, matchInfo.roomType);

        //If the user matched
        if (isOK)
        {
            //Let the user create room
            ZMatchMaking.Instance.OnUserCreateRoom(matchInfo, User);
        }
        else
        {
            //If not, told the user stop create match and display popup.

        }
    }

    //This function uses for implement find match condition
    [Server]
    public bool FindMatchCondition(ZUser user, ZGameRoom.RoomType roomType)
    {
        return true;
    }
    #endregion
}
