﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class ZRegistration : MonoBehaviour
{
    #region INITIAL AND REFERENCE
    void Start () 
    {
        if (ZMasterClient.Instance != null)
        {
            NetworkClient.RegisterHandler<RegistrationCallbackMessage>(ClientRegisterCallback, false);
            this.RegisterListener(ZEventId.OnClientReconnection, (sender, param) => OnClientReconnection());
        }

        //Server handler
        if (ZMasterServer.Instance != null)
        {
            NetworkServer.RegisterHandler<RegistrationMessage>(OnServerReceiveRegisterInfo, false);
        }
    }
    #endregion

    #region CLIENT HANDLE
    void OnClientReconnection()
    {
        NetworkClient.RegisterHandler<RegistrationCallbackMessage>(ClientRegisterCallback, false);
    }

    //Step 1: Send to server
    public void ClientSendNormalRegistration(RegistrationMessage regMsg, ZLogin.LoginType loginType)
    {
        regMsg.loginType = loginType;
        NetworkClient.Send(regMsg);

    }
    public void ClientRegisterCallback(RegistrationCallbackMessage netMsg)
    {
        //Read message
        var regCode = netMsg.regCode;

        switch (regCode)
        {
            //Registration success, using UID to login
            case RegistrationCallbackMessage.RegCode.Successfull:
                //Set connect state OK
                ZMasterClient.Instance.isConnected = true;

                //Post event to send UID to ZGame

                this.PostEvent(ZEventId.OnUserRegisterSuccess, netMsg.UID);
                this.PostEvent(ZEventId.OnUserLoginSuccess, netMsg.UID);
                break;
            case RegistrationCallbackMessage.RegCode.UsernameAlreadyExist:
                this.PostEvent(ZEventId.OnUserRegisterFailed, netMsg.regCode);
                break;
            case RegistrationCallbackMessage.RegCode.EmailAlreadyUsed:
                this.PostEvent(ZEventId.OnUserRegisterFailed, netMsg.regCode);
                break;
            case RegistrationCallbackMessage.RegCode.UnknowError:
                this.PostEvent(ZEventId.OnUserRegisterFailed, netMsg.regCode);
                break;
        }
    }
    #endregion

    #region SERVER HANDLE
    public void OnServerReceiveRegisterInfo(NetworkConnection con,RegistrationMessage netMgs)
    {
        //Check return Code and send back to client
        string checkCode;
        string UID = null;

        Dictionary<string, string> checkCode_UID = ZMasterServer.Instance.Data.DataRegistrationAccount(netMgs);

        if (checkCode_UID.TryGetValue("CheckCode", out checkCode) && checkCode_UID.TryGetValue("UID", out UID))
        {
            Debug.Log("ZRegistration: Client callback check code : " + checkCode + " and UID " + UID);
        }

        switch (checkCode)
        {
            //Registration success, using UID to login
            case "0":
                OnServerSendRegisterCallback(RegistrationCallbackMessage.RegCode.Successfull, UID, con.connectionId);
                break;
            case "1":
                OnServerSendRegisterCallback(RegistrationCallbackMessage.RegCode.UsernameAlreadyExist, null, con.connectionId);
                break;
            case "2":
                OnServerSendRegisterCallback(RegistrationCallbackMessage.RegCode.EmailAlreadyUsed, null, con.connectionId);
                break;
            case "3":
                OnServerSendRegisterCallback(RegistrationCallbackMessage.RegCode.UnknowError, null, con.connectionId);
                break;
        }
    }

    public void OnServerSendRegisterCallback(RegistrationCallbackMessage.RegCode regCode, string UID, int connectionId)
    {
        var msg = new RegistrationCallbackMessage
        {
            regCode = regCode,
            UID = UID
        };

        NetworkServer.connections[connectionId].Send(msg);
    }

    #endregion
}
