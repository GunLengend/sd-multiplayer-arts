﻿using UnityEngine;
using System.Collections;

public enum ZEventId
{
	None = 0,

    //Event Network
    OnUserLoginSuccess,
    OnUserLoginFailed,
    OnUserRegisterFailed,
    OnUserRegisterSuccess,
    OnUserSpawnComplete,
    OnUserReSpawnComplete,
    OnUserLoadDataComplete,
    OnUserDisconnected,
    OnUserLogOut,
    OnUserOnline,
    OnUserOffline,
    OnClientReconnection,

    //Event Select Room
    OnRoomDoesNotExist,
    OnRoomIsFull,
    OnInputWrongPassword,
    OnUserNotHaveEnoughChip,
    OnCannotJoinRoom,

    //Event Match 
    OnGameMatchFound,
    OnGameLoadingMap,
    OnGameConnectedToGameServer,
    OnGameResetMatch,
    OnReceiveBlackjackInfo,
    OnPlayerJoinedRoom,
    OnPlayerLeaveRoom,
    OnJoinRoomWithId,
    OnUserCreateRoom,
    OnGameServerClose,

    //Nha test
    OnSendABC,

    //Event UI
    OnOpenUIBegin,
    OnOpenUIHome,
    OnOpenUIWellcome,
    OnOpenUIListRoom,
    OnOpenUIStartup,
    OnOpenUIDailyReward,
    OnOpenUIUserInfor,
    OnOpenUIListFriends,
    OnOpenUIListChat,

    OnUserClickFindMatch,
    OnUserLoginClickRegister,
    OnUserGetConnectionTimeout,

    OnUserUpdateNickname,
    OnUserUpdateStatusMessage,
    OnUserUpdatePassword,
    OnUserUpdateInfo,
    OnUserUpdateAvatar,
    OnUserUpdateAvatarSuccess,
    OnUserUpdateEmail,
    OnUserUpdateFirstIAP,

    OnUserAddFriend,
    OnUserAddFriendSuccessed,
    OnUserAddFriendFailed,

    OnUserRemoveFriend,
    OnUserRemoveFriendSuccessed,

    OnPlayerAction,
    OnDealerAction,
    OnPlayerBet,

    OnUIGetListRoomAvailable,
    OnUserClickConfirmDailyReward,
    OnUserClickGetCode,
    OnUserSendListGiftCode,
    OnUserSendCLickedGiftCode,
    OnUserUseGiftCode,
    OnUpdateStatusUseGiftCode,
    OnCheckCode,
    OnSendResultCheckCode,
    OnUpdateFriendOnline,
    OnUpdateFriendOffline,
    OnUpdateFriendState,
    OnUpdateConverstation,
    OnSetTopConverstationItem,

    //Event send data for UI
    OnSendBasicInfor,
    OnSendPrivateInfor,
    OnSendGameInfor,
    OnSendListRoomXu,
    OnSendListRoomSao,

    OnReloadListRoomXu,
    OnReloadListRoomSao,

    OnSendFriendInfo,
    OnSendFriendInfor,
    OnSendGuestInfor,
    OnSendUserInfor,
    OnSendListConverstations,
    OnFriendChatOnlineCallback,
    OnSendListChat,
    OnGetListChatContent,
    OnReceiveListChatContent,

    OnAddChatContent,
    OnShowPopupDailyReward,
    OnUserReceiveDailyRewardSuccess,

    OnSendAvatarName,
    OnSendSearchedUser,

    //Event ads
    OnOpenAds,
    OnUpdateStatusAds,
    OnReceiveAdsReward,

    OnPlayerRegisterLeaveRoom,    // User register leave room
    OnPlayerCancelLeaveRoom,      // User cancel leave room

    OnEnableDisconnectPopup,
    OnWaitingForOtherPlayer,

    //Event IAP
    OnEventIAPError,
    OnEventIAPSuccess,
    OnEventIAPX2Success,
    OnBuyProduct1,
    OnBuyProduct2,
    OnBuyProduct3,
    OnBuyProduct4,
    OnBuyProduct5,
    OnBuyProduct6,
    OnBuyProductCustom1,
    OnBuyProductCustom2,
    OnBuyProductCustom3,
    OnBuyProductCustom4,
    OnBuyProductCustom5,

    //Event active email,
    OnCreateOTPCode,
    OnUserEnterOTPCode,
    OTPInCorrect,
    OTPCorrect,
    OnSendInfoForgetPassword,
    OnReceveiResultForgetPassword,
}
