﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class ZEventDispatcher : MonoBehaviour
{
	//Singleton pattern initial
	private static ZEventDispatcher instance;
	public static ZEventDispatcher Instance 
	{ 
		get 
		{ 
			//If instance not exist then create new one
			if(instance == null)
			{
				//Create new game object and add EventDispatcher component
				GameObject singletonEvent = new GameObject();
				instance = singletonEvent.AddComponent<ZEventDispatcher>();
				singletonEvent.name = "Singleton - EventHandle";
			}

			return instance;
		} 
	}

	void Awake()
	{
		//Check if there any instance already exist on scene
		if(instance != null && instance.GetInstanceID() != this.GetInstanceID())
		{
			//Destroy this instance because already exist the singleton of EventDispatcher
			Debug.Log("Instance of Event Handle already exit, destroy this");
			Destroy(gameObject);
		}
		else
		{
			//Set instance
			instance = this as ZEventDispatcher;
			Debug.Log("EventDispatcher Log: Client initial Event Handler");
		}
	}

	void OnDestroy()
	{
		//Reset this static var to null 
		if(instance == this)
			instance = null;

        RemoveRedundancies();
    }

    //Store all listener
    private static Dictionary<ZEventId, List<Action<Component, object>>> listenerDict = new Dictionary<ZEventId, List<Action<Component, object>>>();

	//Register to listen for eventID, callback will be invoke when event with eventID be raise
	public void RegisterListener (ZEventId eventID, Action<Component, object> callback)
	{
		//Check if listener exist in dictionary
		if (listenerDict.ContainsKey(eventID))
		{
			//Add callback to our collection
			listenerDict[eventID].Add(callback);
		}
		else
		{
			//Add new key-value pair
			var listEvent = new List<Action<Component, object>>();
			listEvent.Add(callback);

			listenerDict.Add(eventID, listEvent);
		}
	}

	//Post event, this will notify all listener which register to listen for eventID
	public void PostEvent (ZEventId eventID, Component sender, object param)
	{
		List<Action<Component, object>> actionList;

		if (listenerDict.TryGetValue(eventID, out actionList))
		{
			for (int i = 0, amount = actionList.Count; i < amount; i++)
			{
				try
				{
                    var allDelegates = actionList[i].GetInvocationList();
                    for (int k = 0; k < allDelegates.Length; k++)
                    {
                        bool isNotNull = allDelegates[k] != null;
                        bool isTargetNotNull = !allDelegates[k].Target.Equals(null);
                        if (isNotNull && isTargetNotNull)
                        {
                            allDelegates[k].DynamicInvoke(sender, param);
                        }
                    }
                    // actionList[i](sender, param);
				}
				catch (Exception e)
				{
					Debug.LogFormat("EventDispatcher, exception : {0}", e.Message);
					//Remove listener at i - that cause the exception
					actionList.RemoveAt(i);

					if (actionList.Count == 0)
					{
						//No listener remain, then delete this key
						listenerDict.Remove(eventID);
					}

					//Reduce amount and index for the next loop
					amount--;
					i--;
				}
			}
		}
		else
		{
			//If not exist, just warning, don't throw exceptoin
			Debug.LogWarning("PostEvent, event " +  eventID.ToString() + " no listener for this event");
		}
	}

	//Use for Unregister, not listen for an event anymore.
	public void RemoveListener (ZEventId eventID, Action<Component, object> callback)
	{
		List<Action<Component, object>> actionList;
		if (listenerDict.TryGetValue(eventID, out actionList))
		{
			if (actionList.Contains(callback))
			{
				actionList.Remove(callback);
				if (actionList.Count == 0)// no listener remain for this event
				{
					listenerDict.Remove(eventID);
				}
			}
		}
		else
		{
			//The listeners not exist
			Debug.LogWarning("RemoveListener, event : " + eventID.ToString() + " , no listener found");
		}
	}

	//Clean the ListenerList, remove the listener that have a null target. This happen when an object that already be "delete" in Hirachy, but still have a callback remain in listenerList
	public void RemoveRedundancies()
	{
        List<ZEventId> removes = new List<ZEventId>();
		foreach (var keyPairs in listenerDict)
		{
			var listenerList = keyPairs.Value;
            listenerList.RemoveAll(x => x == null || x.Target.Equals(null));
            if (listenerList.Count == 0)
            {
                removes.Add(keyPairs.Key);
            }
        }
        removes.ForEach(x => listenerDict.Remove(x));
    }

    public void ClearAllListener ()
	{
		listenerDict.Clear();
	}
}

//An Extension class, declare some "shortcut" for using EventDispatcher
public static class ZEventDispatcherExtension
{

	//Use for registering with EventDispatcher
	public static void RegisterListener (this MonoBehaviour sender, ZEventId eventID, Action<Component, object> callback)
	{
		ZEventDispatcher.Instance.RegisterListener(eventID, callback);
	}

    // User for removing with EventDispatcher
    public static void RemoveListener(this MonoBehaviour sender, ZEventId eventID, Action<Component, object> callback)
    {
        ZEventDispatcher.Instance.RemoveListener(eventID, callback);
    }

    //Post event with param
    public static void PostEvent (this MonoBehaviour sender, ZEventId eventID, object param)
	{
		ZEventDispatcher.Instance.PostEvent(eventID, sender, param);
	}

	//Post event with no param (param = null)
	public static void PostEvent (this MonoBehaviour sender, ZEventId eventID)
	{
		ZEventDispatcher.Instance.PostEvent(eventID, sender, null);
	}
}
