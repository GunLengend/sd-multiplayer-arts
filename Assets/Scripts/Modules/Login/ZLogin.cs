﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using SimpleJSON;
using UnityEngine.Networking;

public class ZLogin : MonoBehaviour
{
    public enum LoginType
    {
        Normal,
        Facebook,
        Google,
        Quick
    }

    public struct LastLogin
    {
        public LoginType LastLoginType;
        public string deviceID;
        public string username;
        public string password;
    }

    //variable using analytics login
    public LoginType isLoginType;

    // Use this for initialization
    void Start()
    {
        //Client handler
        if (ZMasterClient.Instance != null)
        {
            NetworkClient.RegisterHandler<LoginCallbackMessage>(ClientLoginCallback, false);
            this.RegisterListener(ZEventId.OnClientReconnection, (sender, param) => OnClientReconnection());
        }

        //Server handler
        if (ZMasterServer.Instance != null)
        {
            NetworkServer.RegisterHandler<LoginMessage>(OnServerReceiveLoginInfo, false);
        }
    }

    #region LOGIN CLIENT
    void OnClientReconnection()
    {
        Debug.Log("ZLogin: Client register network message");
        NetworkClient.RegisterHandler<LoginCallbackMessage>(ClientLoginCallback, false);
    }

    //Login message, username, password got input from user
    public void ClientLoginNormalToServer(string username, string password, LoginType loginType)
    {
        isLoginType = loginType; 

        //Create message
        LoginMessage loginMsg = new LoginMessage
        {
            username = username,
            password = password,
            loginType = loginType
        };

        //Transfer message
        if (ZMasterClient.Instance.isConnected)
           NetworkClient.Send(loginMsg, 2);

        Debug.Log("Login Log: Client login to server");
    }

    public void ClientLoginQuickToServer(string deviceID, ZLogin.LoginType loginType)
    {
        isLoginType = loginType; //set data variable using analytics login
        LoginMessage loginMsg = new LoginMessage
        {
            deviceID = deviceID,
            loginType = loginType
        };

        //Transfer message
        if (ZMasterClient.Instance.isConnected)
        {
            Debug.Log("ZLogin: Client is connected to Server");
            NetworkClient.Send(loginMsg, 2);
        }
        else
        {
            Debug.Log("ZLogin: Client is disconnected to Server");
        }

        Debug.Log("Login Log: Client login to server");
    }

    public void ClientLoginFacebookAccount(string userID, string accessToken, ZLogin.LoginType loginType)
    {
        isLoginType = loginType;

        //TODO: Client send facebook login to server here
        LoginMessage loginMsg = new LoginMessage
        {
            userID = userID,
            accessToken = accessToken,
            loginType = loginType
        };

        //Transfer message
        if (ZMasterClient.Instance.isConnected)
            NetworkClient.Send(loginMsg, 2);

        Debug.Log("Login Log: Client login facebook to server");
    }

    public void ClientLoginGoogleAccount(ZLogin.LoginType loginType)
    {
        isLoginType = loginType;

        //TODO: Client send facebook login to server here
    }

    //Login callback handle
    void ClientLoginCallback(LoginCallbackMessage loginClbMsg)
    {
        //Read message
        var authCode = loginClbMsg.authCode;

        //Check login authCode
        switch (authCode)
        {
            case LoginCallbackMessage.AuthCode.Sucessfull:
                Debug.Log("Login Log : Client Log: Client login success");

                //Set connect state OK
                ZMasterClient.Instance.isConnected = true;

                //Post event to send UID to ZGame
                this.PostEvent(ZEventId.OnUserLoginSuccess, loginClbMsg.UID);
                break;
            case LoginCallbackMessage.AuthCode.AccountLogined:
                Debug.Log("Login Log : Client Log: Client login in other location");
                this.PostEvent(ZEventId.OnUserLoginFailed, LoginCallbackMessage.AuthCode.AccountLogined);

                break;
            case LoginCallbackMessage.AuthCode.WrongUserOrPass:
                string log = "Login Log : Client Log: Wrong username";
                this.PostEvent(ZEventId.OnUserLoginFailed, LoginCallbackMessage.AuthCode.WrongUserOrPass);
                Debug.Log(log);
                break;
            case LoginCallbackMessage.AuthCode.AccountNotExist:
                this.PostEvent(ZEventId.OnUserLoginFailed, LoginCallbackMessage.AuthCode.AccountNotExist);
                Debug.Log("Login Log : Wrong password");
                break;
            case LoginCallbackMessage.AuthCode.UnknowError:
                this.PostEvent(ZEventId.OnUserLoginFailed, LoginCallbackMessage.AuthCode.UnknowError);
                Debug.Log("Login Log : Unknown error");
                break;
        }
    }
    #endregion

    #region LOGIN SERVER

    //Client login to server
    void OnServerReceiveLoginInfo(NetworkConnection con, LoginMessage loginMsg)
    {
        if (loginMsg.loginType == LoginType.Normal)
        {
            ServerLoginNormal(loginMsg.username, loginMsg.password, con.connectionId);
        }
        if (loginMsg.loginType == LoginType.Facebook)
        {
            StartCoroutine(ServerLoginFacebook(loginMsg.userID, loginMsg.accessToken, con.connectionId));
        }
        if (loginMsg.loginType == LoginType.Quick)
        {
            ServerLoginQuick(loginMsg.deviceID, con.connectionId);
        }
        if (loginMsg.loginType == LoginType.Google)
        {
            //TODO: server login Google
            //ServerLoginNormal(netMsg);
        }
    }

    //Server login nomal
    private void ServerLoginNormal(string username, string password, int connectionID)
    {
        Dictionary<string, string> checkCode_UID = new Dictionary<string, string>();
        string UID = null;

        //Read username and password from client
        checkCode_UID = ZMasterServer.Instance.Data.DataCheckLogin(username, password, "", "", "", LoginType.Normal);

        if (checkCode_UID.TryGetValue("CheckCode", out string checkCode) && checkCode_UID.TryGetValue("UID", out UID))
        {
            Debug.Log("ZLogin/Server: Client callback check code Login normal : " + checkCode + " and UID " + UID);
        }
        switch (checkCode)
        {
            //Connect successful    
            case "0":
                //Call function to send login callback
                SendClientLoginCallback(LoginCallbackMessage.AuthCode.Sucessfull, UID, connectionID);
                break;
            //Wrong username or password
            case "1":
                SendClientLoginCallback(LoginCallbackMessage.AuthCode.WrongUserOrPass, UID, connectionID);
                break;
            //Account not exist
            case "2":
                SendClientLoginCallback(LoginCallbackMessage.AuthCode.AccountNotExist, UID, connectionID);
                break;
            //Unknown error
            case "3":
                SendClientLoginCallback(LoginCallbackMessage.AuthCode.UnknowError, UID, connectionID);
                break;
        }
    }

    //Server Login Quick
    private void ServerLoginQuick(string deviceID, int connectionID)
    {
        string UID = null;
        var checkCode_UID = ZMasterServer.Instance.Data.DataCheckLogin("", "", "", "", deviceID, LoginType.Quick);
        if (checkCode_UID.TryGetValue("CheckCode", out string checkCode) && checkCode_UID.TryGetValue("UID", out UID))
        {
            Debug.Log("ZLogin/Server: Client callback check code Login quick : " + checkCode + " and UID " + UID);
        }
        switch (checkCode)
        {
            //Connect successul
            case "0":
                //Call function to send login callback
                SendClientLoginCallback(LoginCallbackMessage.AuthCode.Sucessfull, UID, connectionID);
                break;
            ////Wrong username or password
            case "1":
                SendClientLoginCallback(LoginCallbackMessage.AuthCode.WrongUserOrPass, UID, connectionID);
                break;
            //Account not exist
            case "2":
                //Create Login quick
                CreateLoginQuick(deviceID, "", "", "", "", "", "", connectionID);
                this.PostEvent(ZEventId.OnUserRegisterSuccess);
                break;
            //Unknow Error
            case "3":
                SendClientLoginCallback(LoginCallbackMessage.AuthCode.UnknowError, UID, connectionID);
                break;
        }
    }

    private void CreateLoginQuick(string deviceID, string name, string gender, string urlAvatar, string about, string email, string address, int connectionID)
    {
        string UID = null;
        RegistrationMessage dataRegister = new RegistrationMessage
        {
            idDevice = deviceID,
            name = name,
            gender = gender,
            urlAvartar = urlAvatar,
            about = about,
            email = email,
            address = address,
            loginType = LoginType.Quick
        };

        var checkCode_UID = ZMasterServer.Instance.Data.RegisterQuick(dataRegister);

        if (checkCode_UID.TryGetValue("CheckCode", out string checkCode) && checkCode_UID.TryGetValue("UID", out UID))
        {
            Debug.Log("ZLogin/Server: Client callback check code Login Quick : " + checkCode + " and UID " + UID);
        }
        switch (checkCode)
        {
            //Connect successful 
            case "0":
                SendClientLoginCallback(LoginCallbackMessage.AuthCode.Sucessfull, UID, connectionID);
                break;
            ////Wrong username or password
            case "1":
                SendClientLoginCallback(LoginCallbackMessage.AuthCode.WrongUserOrPass, UID, connectionID);
                break;
            //Acccount not exist
            case "2":
                SendClientLoginCallback(LoginCallbackMessage.AuthCode.AccountNotExist, UID, connectionID);
                break;
            //Unknow Error
            case "3":
                SendClientLoginCallback(LoginCallbackMessage.AuthCode.UnknowError, UID, connectionID);
                break;
        }
    }

    //Server Login Facebook
    IEnumerator ServerLoginFacebook(string userid, string token, int connectionID)
    {
        using UnityWebRequest www = UnityWebRequest.Get("https://graph.facebook.com/me?fields=id,name,email,picture{url},gender,address,about&access_token=" + token);
        yield return www.SendWebRequest();

        if (www.result == UnityWebRequest.Result.ConnectionError || www.result == UnityWebRequest.Result.ProtocolError)
        {
            Debug.Log(www.error);
        }
        else
        {
            // Show results as text
            Debug.Log(www.downloadHandler.text);
            var N = JSON.Parse(www.downloadHandler.text);
            Debug.Log("try get value: " + JSON.TryGetValue("id", N));

            //valid access token
            if (userid == N["id"].Value)
            {
                Dictionary<string, string> checkCode_UID = new Dictionary<string, string>();
                string checkCode;
                string UID = null;
                checkCode_UID = ZMasterServer.Instance.Data.DataCheckLogin("", "", userid, "", "", LoginType.Facebook);

                if (checkCode_UID.TryGetValue("CheckCode", out checkCode) && checkCode_UID.TryGetValue("UID", out UID))
                {
                    Debug.Log("ZLogin/Server: Client callback check code Login normal : " + checkCode + " and UID " + UID);
                }

                switch (checkCode)
                {
                    //Connect successful    
                    case "0":
                        //Call function to send login callback
                        SendClientLoginCallback(LoginCallbackMessage.AuthCode.Sucessfull, UID, connectionID);
                        break;
                    //Wrong username or password
                    case "1":
                        SendClientLoginCallback(LoginCallbackMessage.AuthCode.WrongUserOrPass, UID, connectionID);
                        break;
                    //Account not exist
                    case "2":
                        //created login facebook
                        CreatedLoginFacebook(userid, JSON.TryGetValue("name", N), JSON.TryGetValue("gender", N), N["picture"]["data"]["url"], JSON.TryGetValue("about", N), JSON.TryGetValue("email", N), JSON.TryGetValue("address", N), connectionID);
                        this.PostEvent(ZEventId.OnUserRegisterSuccess);

                        //SendClientLoginCallback(LoginCallbackMessage.AuthCode.AccountNotExist, UID, connectionID);
                        break;
                    //Unknown error
                    case "3":
                        SendClientLoginCallback(LoginCallbackMessage.AuthCode.UnknowError, UID, connectionID);
                        break;
                }
            }
            //not valid access token
            else
            {
                //error token
                SendClientLoginCallback(LoginCallbackMessage.AuthCode.UnknowError, userid, connectionID);
            }
        }
    }

    private void CreatedLoginFacebook(string userid, string name, string gender, string urlAvatar, string about, string email, string address, int connectionID)
    {
        string UID = null;
        RegistrationMessage dataRegister = new RegistrationMessage
        {
            idFacebook = userid,
            name = name,
            gender = gender,
            urlAvartar = urlAvatar,
            about = about,
            email = email,
            address = address,
            loginType = LoginType.Facebook
        };

        var checkCode_UID = ZMasterServer.Instance.Data.RegisterFacebook(dataRegister);
        if (checkCode_UID.TryGetValue("CheckCode", out string checkCode) && checkCode_UID.TryGetValue("UID", out UID))
        {
            Debug.Log("ZLogin/Server: Client callback check code Login facebook : " + checkCode + " and UID " + UID);
        }
        switch (checkCode)
        {
            //Connect successful    
            case "0":
                //Call function to send login callback
                SendClientLoginCallback(LoginCallbackMessage.AuthCode.Sucessfull, UID, connectionID);
                break;
            //Wrong username or password
            case "1":
                SendClientLoginCallback(LoginCallbackMessage.AuthCode.WrongUserOrPass, UID, connectionID);
                break;
            //Account not exist
            case "2":
                SendClientLoginCallback(LoginCallbackMessage.AuthCode.AccountNotExist, UID, connectionID);
                break;
            //Unknown error
            case "3":
                SendClientLoginCallback(LoginCallbackMessage.AuthCode.UnknowError, UID, connectionID);
                break;
        }
    }

    //Server send login status
    void SendClientLoginCallback(LoginCallbackMessage.AuthCode authCode, string UID, int connectionId)
    {
        LoginCallbackMessage loginClbMsg = new LoginCallbackMessage
        {
            authCode = authCode
        };

        if (authCode == LoginCallbackMessage.AuthCode.Sucessfull)
        {
            //ZMasterServer.Instance.Data.UpdateStatusLogin(UID, true);
            bool isLoggedIn = ZMasterServer.Instance.UserManager.usersOnline.Exists(x => x.UID == UID);
            if (isLoggedIn)
            {
                loginClbMsg.authCode = LoginCallbackMessage.AuthCode.AccountLogined;
            }
        }

        loginClbMsg.UID = UID;
        NetworkServer.connections[connectionId].Send(loginClbMsg);
    }

    #endregion
}
