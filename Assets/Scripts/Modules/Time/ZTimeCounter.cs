﻿using UnityEngine;
using System.Collections;

/// <summary>
/// This modules using to add time counter in second and return the result when ending
/// to function OnCounterComplete. To uses this modules simply add this component and reference to a 
/// variable "x" then listen result by x.OnCounterComplete += YourFunctionToHandle()
/// </summary>

public class ZTimeCounter : MonoBehaviour
{
    public System.Action OnCounterCompleted;
    public System.Action<float> OnUpdateCounter;
    public System.Action<float> OnStepCounter;

    private float timeCounter;
    private float stepTime; // We wil handle this for each second time

    public bool IsCounting { get; private set; }

    public float GetCurrentTime()
    {
        return timeCounter;
    }

    public void StartCounter(float time)
    {
        StopCounter();

        timeCounter = time;
        stepTime = time;
        IsCounting = true;
        StartCoroutine(CountDownTime());
    }

    public void StopCounter()
    {
        IsCounting = false;
        if (OnUpdateCounter != null) OnUpdateCounter(0);
        StopCoroutine("CountDownTime");
    }

    IEnumerator CountDownTime()
    {
        while (stepTime >= 0)
        {
            yield return new WaitForSeconds(1.0f);

            if (OnStepCounter != null)
                OnStepCounter(stepTime);

            stepTime -= 1;
        }
    }

    public void Update()
    {
        if (IsCounting)
        {
            timeCounter -= Time.deltaTime;

            if (OnUpdateCounter != null)
                OnUpdateCounter(timeCounter);

            if (timeCounter <= 0)
            {
                if (OnCounterCompleted != null) 
                {
                    StopCounter();
                    OnCounterCompleted();
                }
            }
        }
    }
}
