﻿using System;
using System.Collections.Generic;

public static class ZParse
{
    /// <summary>
    /// This function convert the string into T type. Ex: Login.state = ZParse.ZEnum<LoginState>("Online");
    /// <para>Will convert "Online" into LoginState and assign to "state" variable.</para>
    /// </summary>
    /// <typeparam name="T">T can be any type</typeparam>
    /// <param name="value">String value to cast out as a T type</param>
    /// <returns></returns>
    public static T ZEnum<T>(string value)
	{
		return (T)Enum.Parse(typeof(T), value, true);
    }

    /// <summary>
    /// This function help to foreach all the element of the "enumerable" param and allow do an action in "func"
    /// <para>and then required to return a bool value. Usage similar like List.ForEach by System.Linq but iff the value return true,</para> 
    /// <para>this function will stop the loop imediately and will not reach other remain item.</para>
    /// Ex:     ZParse.ForEach(List, item => { if(item.property == condition) return true; return false; });
    /// </summary>
    /// <typeparam name="T">T can be any type</typeparam>
    /// <param name="enumerable">A list, enum or enumerable type that you need to foreach elements </param>
    /// <param name="func">Function required return a bool value, true for break the loop, false to continue reach all the remain </param>
	public static void ForEach<T>(this IEnumerable<T> enumerable, Func<T, bool> func)
	{
		foreach (var cur in enumerable)
		{
			if (!func(cur))
			{
				break;
			}
		}
	}
}
