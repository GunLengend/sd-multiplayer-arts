﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ZGlobalAsset : MonoBehaviour 
{   
    public GameObject userAsset;
    public GameObject playerAsset;
	public GameObject dataTransAsset;

    //Singleton pattern initial
    private static ZGlobalAsset instance;
    public static ZGlobalAsset Instance { get { return instance; } }

    void Awake()
    {
        DontDestroyOnLoad(this);

        //This prevent instance double when load new scene or back to current scene
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
}
