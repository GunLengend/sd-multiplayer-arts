﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct MatchInfo 
{
    public ZGameRoom.RoomGame roomInfo;
    public int maxPlayerConnect;
    public string playerToken;
    public string mapName;
}

public struct FindMatchInfo
{
    public ZGameRoom.RoomType roomType;
    public int findCondition;
}

public struct CreateMatchInfo
{
    public ZGameRoom.RoomType roomType;
    public int numberPlayer;
    public int joinCondition;
    public string roomPassword;
}

public struct JoinMatchInfo
{
    public int roomId;
    public ZGameRoom.RoomType roomType;
    public int joinCondition;
    public string roomPassword;
}

[System.Serializable]
public struct RoomInfo
{
    public int roomID;
    public int numPlayer;
    public int maxPlayer;
    public bool isHasPassword;
    public ZGameRoom.RoomState roomState;
    public ZGameRoom.RoomType roomType;
    public ZGameRoom.Channel roomChannel;
}



[System.Serializable]
public struct PlayerInfoContainer
{
    public ZGameInfo.GameInfo GameInfo;
    public ZUser.BasicInfo BasicInfo;
}






