﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GMask : MonoBehaviour
{
    [SerializeField]private RectTransform parentWindow;
    [SerializeField]private Canvas canvasOrder;
    [SerializeField]private GraphicRaycaster canvasRay;
    [SerializeField]private string sortLayerName = "Default";
    [SerializeField]private int sortOrder = 4;

    private void OnEnable()
    {
        GetComponent<GMask>();
        OnOpen();
    }

    private void OnDisable()
    {
        OnClose();
    }

    public void OnOpen()
    {
        parentWindow = GetComponent<RectTransform>();
        InitCanvasOrder(parentWindow);
        SetOrder();
    }

    public void OnClose()
    {
        RemoveOnSafe();
    }

    private void InitCanvasOrder(RectTransform parentWindow)
    {
        if (parentWindow.GetComponent<Canvas>())
        {
            return;
        }

        canvasOrder = gameObject.AddComponent<Canvas>();
        canvasRay = gameObject.AddComponent<GraphicRaycaster>();
    }

    private void SetOrder()
    {
        if (canvasOrder)
        {
            canvasOrder.overrideSorting = true;
            canvasOrder.sortingLayerName = sortLayerName;
            canvasOrder.sortingOrder = sortOrder;
        }
    }

    private void RemoveOnSafe()
    {
        if (IsSafe(parentWindow))
        {
            Destroy(GetComponent<GraphicRaycaster>());
            Destroy(GetComponent<Canvas>());
        }
    }

    private bool IsSafe(RectTransform _parentWindow)
    {
        bool isSafe = true;

        foreach (RectTransform rect in _parentWindow)
        {
            Canvas childCanvas = rect.GetComponent<Canvas>();
            if (childCanvas != null)
            {
                return false;
            }
        }

        return isSafe;
    }
}
