﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ZConfig
{
    //Networking config
    public const string masterServerIP = "127.0.0.1";
    //public const string masterServerIP = "139.180.138.198";
    public const int masterServerPort = 8888;
    public const int maxConnection = 5000;
    public const int minGameRoomPort = 4000;
    public const uint defaultDisconnectTimeOut = 2000;

    //System config
    public const int defaultMaxRoomPlayer       = 5;
    public const int defaultMinRoomPlayer       = 1;
    public const int maximumRoomAllowed = 1000;
    public const int defaultRoomQuanity = 1;
    public const int defaultRoomMMR = 0;
    public const int defaultPrepareTime = 5;
    public const int defaultConnectionAttempt = 5;

    public const float disconnectTimeOutPlaying = 30f;
    public const float disconnectTimeOutWaiting = 5f;

    //Match config
    public const float TIME_BET = 10;
    public const float TIME_ACTION = 10;
    public const float TIME_DELAY_PREPARE = 2.0f;
    public const float TIME_DELAY_HIT_CARD = 1.5f;
    public const float TIME_DELAY_DIVITION_CARD = 2.5f;
    public const float TIME_DELAY_FOR_SWITCH_HOST = 2.0f;
    public const float TIME_DELAY_FOR_CLEAR_CARD = 2.0f;
    public const float TIME_DELAY_SEND_BLACKJACK_INFO = 3.0f;
    public const float TIME_DELAY_FOR_CHECK_RESULT = 3.0f;
    public const float TIME_COUNT_NO_HOST_FULL = 10.0f;
    public const float TIME_COUNT_NO_HOST_WAIT = 60.0f;

    //Game room config
    public const int defaultMaxSeatInRoom = 5;

    public const string gameRoomPath = "\"D:\\Development\\[SD] Multiplayer - ARTS\\Build\\GameServer\\GameServer.exe\"";        //Path of Server 
    public const string argsParam1 = "-nographics";
    public const string argsParam2 = "-batchmode";

    //Enable when publishing, change any value required to build GameServer and MasterServer
    public const bool ENABLE_AUTO_PORT_FORWARDING = false;
    public const bool ENABLE_AUTO_OPEN_GAMESERVER = true;
    public const bool ENABLE_TEAM_MODE = false;
    public const bool ENABLE_TEST_SPAWNED_GAMESERVER = false;
    public const bool ENABLE_AUTO_CREATE_DEFAULT_ROOM = false;

    //Url Upload and Download image 
    public const string urlUploadImage = "http://139.180.138.198/uploadimage.php";
    public const string urlDownloadImage = "http://139.180.138.198/downloadimage.php?UID=";

    //Account sendmail
    public const string Email_Send = "sup.orbitstudio@gmail.com";
    public const string Email_Username = "sup.orbitstudio";
    public const string Email_Password = "Reddevil1!@";

    public const string Url_This_Game = "https://play.google.com/store/apps/details?id=com.orbits.xizach3d";
    public const string Url_Facebook_Fanpage = "https://www.facebook.com/xizach3d";
}
