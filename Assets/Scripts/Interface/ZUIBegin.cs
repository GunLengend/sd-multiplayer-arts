using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ZUIBegin : MonoBehaviour
{
    public Button quickLoginButton;
    public GameObject content;

    private void Start()
    {
        quickLoginButton.onClick.AddListener(OnQuickLogin);
        this.RegisterListener(ZEventId.OnUserSpawnComplete, (sender, param) => HideBeginUI());
    }

    private void HideBeginUI()
    {
        content.SetActive(false);
    }

    public void OnQuickLogin()
    {
        ZMasterClient.Instance.Login.ClientLoginQuickToServer("abc", ZLogin.LoginType.Quick);
    }
}
