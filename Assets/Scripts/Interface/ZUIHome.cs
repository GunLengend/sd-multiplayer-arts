using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class ZUIHome : MonoBehaviour
{
    public GameObject content;
    public Button quickPlay;

    // Start is called before the first frame update
    void Start()
    {
        quickPlay.onClick.AddListener(OnQuickPlayButtonClick);
        this.RegisterListener(ZEventId.OnUserSpawnComplete, (sender, param) => DisplayLobbyUI());
    }

    private void OnQuickPlayButtonClick()
    {
        FindMatchInfo findData = new FindMatchInfo
        {
            findCondition = 1,
            roomType = ZGameRoom.RoomType.Normal
        };

        this.PostEvent(ZEventId.OnUserClickFindMatch, findData);
    }

    private void DisplayLobbyUI()
    {
        content.SetActive(true);   
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
