﻿#if ENABLE_MATCH_MAKING_MODE
using System;
using UnityEngine;
using Mirror;
using System.Collections.Generic;

/// <summary>
/// This is GameRoom create by ZGameRoom on MatchMaking system, this contain
/// connection between MatchMaking server and player on game match.
/// </summary>

public class ZGameServer : MonoBehaviour
{
    [Header("Game Server Info")]
    public ZGameRoom.RoomGame roomConnectInfo;
    public ZGameRoom.RoomType roomType;

    public int roomID;
    public int currentConnection;
    public int maxConnection;

    public Transport transport;
    public ZDataTransfer Transfer;
    public List<ZPlayer> disconnectPlayers = new List<ZPlayer>();

    //Singleton pattern initial
    private static ZGameServer instance;
    public static ZGameServer Instance { get { return instance; } }

    #region INITIAL AND ASSIGN
    void Awake()
    {
        //If enable, it will pass all argument command line into parameter 
        if (ZConfig.ENABLE_AUTO_OPEN_GAMESERVER)
        {
            string[] arguments = Environment.GetCommandLineArgs();

            for (int i = 0; i < arguments.Length; i++)
            {
                Debug.Log("Argument " + i + " is " + arguments[i]);
            }

            roomConnectInfo.IP = arguments[3];
            roomConnectInfo.port = int.Parse(arguments[4]);
            roomType = ZParse.ZEnum<ZGameRoom.RoomType>(arguments[5]);
            maxConnection = int.Parse(arguments[6]);
            roomID = int.Parse(arguments[7]);

            Debug.Log($"Server Ip: {ZConfig.masterServerIP}, Server Port: {ZConfig.masterServerPort}, " +
                $"RoomIP: {roomConnectInfo.IP}, RoomPort: {roomConnectInfo.port}, " +
                $"RoomType: {roomType}, MaxConnect: { maxConnection}, roomID: {roomID}");
               
        }
        //Else get default setting below
        else
        {
            roomConnectInfo.IP = "127.0.0.1";
            roomConnectInfo.port = 4001;
            roomID = 1;

            //REMEMBER: Max connection allower connect to this Game Server
            maxConnection = ZConfig.defaultMaxRoomPlayer;
        }

        if (instance == null)
            instance = this;
        else
            Destroy(gameObject);
    }

    // Use this for initialization
    void Start()
    {
        Initialized();
    }

    private void Initialized()
    {
        Debug.Log("ZGameServer : IP: " + roomConnectInfo.IP + " Port: " + roomConnectInfo.port + " Match type: " + roomType);

        //System handle for player
        NetworkServer.OnConnectedEvent = OnClientConnected;
        NetworkServer.OnDisconnectedEvent = OnClientDisconnected;
        NetworkServer.RegisterHandler<ObjectAddTypeMessage>(OnServerAddPlayer);
        NetworkServer.RegisterHandler<ReconnectMessage>(OnClientReconnected);

        //Event handle
        this.RegisterListener(ZEventId.OnGameServerClose, (sender, param) => OnGameServerClose());

        //Transport initialize
        transport.SetServerPort((ushort)roomConnectInfo.port);
        Transport.activeTransport = transport;

        //Start listen
        NetworkServer.Listen(maxConnection);

        Debug.Log("ZGameServer: Game Server initialized, listen port: " + roomConnectInfo.port);
    }

    #endregion

    #region CONNECTION HANDLER

    //This function call when player connect to this Game Server
    void OnClientConnected(NetworkConnection con)
    {
        //Rise up current connection
        currentConnection++;

        Debug.Log("ZGameServer: current " + currentConnection + " player connected to Game Server");
    }

    //This function call when player disconnect to this Game Server
    public void OnClientDisconnected(NetworkConnection con)
    {
        currentConnection--;

        //If there is no identity of this connection, return.
        if (con.identity == null)
            return;

        //Find disconnect user.
        var disconnectedPlayer = con.identity.gameObject.GetComponent<ZPlayer>();
        if (disconnectedPlayer != null)
        {
            //If find out, add to list disconnect players.
            disconnectedPlayer.isDisconnected = true;
            disconnectPlayers.Add(disconnectedPlayer);

            Debug.Log("ZGameServer: Player named " + disconnectedPlayer.BasicInfo.nickname + " disconnected!");
        }
        else
            return;
    }

    //This function call when player reconnect to this Game Server
    public void OnClientReconnected(NetworkConnection con, ReconnectMessage netMsg)
    {
        //Read message
        var assetId = netMsg.assetId;

        //Find out which player is disconnected.
        ZPlayer player = disconnectPlayers.Find(x => x.playerToken == netMsg.syncCondition);

        //Remove it out of list disconnected
        disconnectPlayers.Remove(player);

        //Replace the player disconnect to new connection, now it can receive the data again.
        NetworkServer.ReplacePlayerForConnection(con, con.identity.gameObject, assetId);
        player.isDisconnected = false;

        Debug.Log("ZGameServer: The player with name " + player.BasicInfo.nickname + " is reconnected!");
    }
    #endregion

    #region SYSTEM HANDLE
    //This is Player spawn handler
    void OnServerAddPlayer(NetworkConnection con, ObjectAddTypeMessage netMsg)
    {
        Debug.Log("ZGameServer: Game Server adding player...");

        //Spawn player 
        GameObject playerPrefabs = ZGlobalAsset.Instance.playerAsset;
        GameObject player = Instantiate(playerPrefabs) as GameObject;

        //Set the player transform to be child of this game object.
        player.transform.parent = this.transform;

        //Add player message send back to client, spawn handler on client will launch
        NetworkServer.AddPlayerForConnection(con, player);
        Debug.Log("ZGameServer: Game Server added player!");

        //Post Event to add player into list allPlayer
        this.PostEvent(ZEventId.OnPlayerJoinedRoom, player);
    }

    //This function call when receive OnGameServerClose event, uses for close server application
    public void OnGameServerClose()
    {
        //Shutdown network and close application
        Debug.Log("ZGameServer: Disconnect all players and shutdown network connection... Call NetworkServer.Shutdown");
        NetworkServer.Shutdown();

        Debug.Log("ZGameServer: Close Game Server! Call Application.Quit");
        Application.Quit();
    }

    //This function uses for remove player and destroy it on GameServer.
    public void OnServerRemovePlayer(ZPlayer player)
    {
        Debug.Log("ZGameServer: Server remove Player named: " + player.BasicInfo.nickname + " out of room");

        //Next, disconnect and destroy that player on all client and Game Server.
        player.connectionToClient.Disconnect();
        NetworkServer.DestroyPlayerForConnection(player.connectionToClient);

        Debug.Log("ZGameServer: Server destroy Player named: " + player.BasicInfo.nickname + " and call Transfer.CmdOnPlayerLeaveRoom");

        //Aftet that, tell the player leave room via DataTransfer
        Transfer.CmdOnPlayerLeaveRoom(player.playerToken);
    }

    //This function uses for disconnect all player.
    public void OnDisconnectAll()
    {
        NetworkServer.DisconnectAll();
    }
    #endregion
}

#endif