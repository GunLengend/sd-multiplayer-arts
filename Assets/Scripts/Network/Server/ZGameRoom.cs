﻿#undef ENABLE_TEAM_MODE
using UnityEngine;
using System.Collections.Generic;
using Mirror;

//Handle room in hidden lobby on server
//Create by Server via MatchMaking module
//This contain all MatchGame information
//To start a GameServer for this room.

public class ZGameRoom : MonoBehaviour
{
    public enum Channel
    {
        Beginner,
        Intermediate,
        Master,
        Expert
    }

    [Header("Room Info")]
    public int roomID;                              //The ID of the room
    public int currentRoomPlayer;                   //Current player in room
    public int maxRoomPlayer;                       //Maximum room player joining allowed
    public int minRoomPlayer;                       //Minimum room player to set room ready                    
    public string roomPassword;                     //Password to join room.
    public bool isRoomReady;                        //Is room ready or not
    public bool isHasPassword;                      //Is room has password or not

    public RoomState roomState;                     //State of the room
    public RoomType roomType;                       //Type of the room

    [Header("Game Info")]
    public Channel roomChannel;                     //Channel of the room
    public int matchMakingPoint;                    //Match making point to determine join room condition

    [Header("Connect Info")]
    public RoomGame roomConnectInfo;                //The connection info to connect to game server linked with this room

    public List<ZUser> allUser;                     //All user are in room

#if ENABLE_TEAM_MODE
	public List<ZUser> redTeam;
	public List<ZUser> blueTeam;
#endif

    public ZDataTransfer Transfer;                  //Reference of DataTransfer between GameServer and MasterServer
    private string gameServerCmd;                   //Command line to start GameServer
    private string portForwardCmd;                  //Command line to start open firewall rules.

#if UNITY_EDITOR
    public GameObject gServerPrefabs;
#endif

    [System.Serializable]
    public enum RoomState
    {
        Empty,
        Full,
        Playing,
        Waiting,
        Preparing,
        Finishing
    }

    [System.Serializable]
    public enum RoomType
    {
        Normal, Tournament
    }

    [System.Serializable]
    public struct RoomGame
    {
        public string IP;
        public int port;
    }

    #region CREATE & ASSIGN
    //This create by server	default create fuction
    public void Start()
    {
        //If room create auto without player
        if (currentRoomPlayer == 0)
        {
            allUser = new List<ZUser>();
            OnRoomChangeState(RoomState.Empty);
        }

        //NOTE: Match condidtion and info initial here
        matchMakingPoint = 0;

        //Event handle
        this.RegisterListener(ZEventId.OnUserDisconnected, (sender, param) => OnPlayerLeaveRoom((ZUser)param));
    }

    //This create by match making on match finding or by user
    public void OnRoomCreate(ZUser user)
    {
        //Initial list user in room and add that user to room.
        allUser = new List<ZUser>
        {
            user
        };

        //Mark as user already joined room
        user.isJoinedRoom = true;

        //Count current player
        currentRoomPlayer = allUser.Count;
        user.transform.parent = this.transform;

        //Set room state
        OnRoomChangeState(RoomState.Waiting);
        isHasPassword = !string.IsNullOrEmpty(roomPassword);

        Debug.Log("GameRoom: Room with ID: " + roomID + " created!");

        //Set room ready for the first time
        if (!isRoomReady)
        {
            //If current room user at least equal to required user in match, let it sync data complete.
            if (currentRoomPlayer == minRoomPlayer)
                OnRoomReady();
        }
    }

    public Channel OnRoomSetChannel(int mmP)
    {
        Channel channel = Channel.Beginner;

        if (mmP <= 100)
            channel = Channel.Beginner;
        else if (mmP > 100 && mmP <= 500)
            channel = Channel.Intermediate;
        else if (mmP > 500 && mmP <= 1500)
            channel = Channel.Expert;
        else
            channel = Channel.Master;

        return channel;
    }

    #endregion

    #region PLAYER HANDLER
    public void OnPlayerJoinRoom(ZUser user)
    {
        //Add user to list users
        allUser.Add(user);

        //Mark as user already join room
        user.isJoinedRoom = true;

        //Calculated current player
        currentRoomPlayer = allUser.Count;

        //And move player to this room, just for easy display
        user.transform.parent = this.transform;

        //Set room ready for the first time
        if (!isRoomReady)
        {
            //If current room user at least equal to required user in match, let it sync data complete.
            if (currentRoomPlayer == minRoomPlayer)
                OnRoomReady();
        }
        else
        {
            //NOTE Prevent join 1: Add user when room have slot 
            if (currentRoomPlayer < maxRoomPlayer)
                OnRoomAddPlayer(user);
            else
            {
                //Or can't join
                OnRoomChangeState(RoomState.Full);

                //Then remove it out of this room
                allUser.Remove(user);

                user.isJoinedRoom = false;

                currentRoomPlayer = allUser.Count;
                user.transform.parent = ZMasterServer.Instance.transform;

                Debug.Log("GameRoom.cs : Room is full, can't join the game");
                return;
            }
        }
    }

    public void OnPlayerLeaveRoom(ZUser leftPlayer)
    {
        //Remove out of list
        allUser.Remove(leftPlayer);

        //Set the states of the player
        leftPlayer.isJoinedRoom = false;
        leftPlayer.transform.parent = ZMasterServer.Instance.transform;

        //Reduce the current room player.
        currentRoomPlayer--;

        //Tell all user rebuild observer list
        allUser.ForEach(x => NetworkServer.RebuildObservers(x.netIdentity, true));

        //Remove on UserMananager.
        ZMasterServer.Instance.UserManager.RemoveUserOffline(leftPlayer);

        //Then destroy the user to let it spawn another one, see ZGame.OnReconnectLobby
        NetworkServer.DestroyPlayerForConnection(leftPlayer.connectionToClient);

        Debug.Log("ZGameRoom: Remove user and destroy cause it leave room");

        if (currentRoomPlayer == 0)
            OnNoUserOnRoom();
    }

    public void OnPlayerWasDestroy()
    {
        //Find all user was destroyed
        var nullUser = allUser.FindAll(x => x == null);

        //Remove all null user
        allUser.RemoveAll(x => nullUser.Contains(x));

        //Reduce the current room player
        currentRoomPlayer -= nullUser.Count;

        //Tell all user rebuild observer list
        allUser.ForEach(x => NetworkServer.RebuildObservers(x.netIdentity, true));

        Debug.Log("ZGameRoom: Remove destroyed user and clear it cause it not founded");

        if (currentRoomPlayer == 0)
            OnNoUserOnRoom();
    }

    //This function call when room already playing and player join middle the game
    public void OnRoomAddPlayer(ZUser user)
    {
        //Tell that user generate player token
        user.GeneratePlayerToken();

        //Send the connection info to that player
        user.Match.TargetOnGameMatchFound(user.connectionToClient, roomConnectInfo, maxRoomPlayer, user.playerToken);

        //If that room not playing, let it build list observer and spawn user in client.
        if (roomState != RoomState.Playing)
        {
            //Tell all user rebuild observer list
            allUser.ForEach(x => NetworkServer.RebuildObservers(x.netIdentity, true));
        }

        //Remove this player in game out of match making 
        ZMatchMaking.Instance.Queue.Remove(user);

        Debug.Log("ZGameRoom: ");
    }
    #endregion

    #region ROOM HANDLER
    public void OnRoomReady()
    {
        if (ZConfig.ENABLE_AUTO_PORT_FORWARDING)
        {
            StartOpenGamePort();
        }

        if (ZConfig.ENABLE_AUTO_OPEN_GAMESERVER)
        {
            StartGameRoom();
        }
     
        if (ZConfig.ENABLE_TEAM_MODE)
        {
            //TODO: Team dividing go here.
        }

        //Set client match found
        for (int i = 0; i < currentRoomPlayer; i++)
        {
            //Tell all user generate player token.
            allUser[i].GeneratePlayerToken();

            //Send client match found and force user rebuild observer to see list of custom user
            allUser[i].Match.TargetOnGameMatchFound(allUser[i].connectionToClient, roomConnectInfo, maxRoomPlayer, allUser[i].playerToken);
            NetworkServer.RebuildObservers(allUser[i].netIdentity, true);

            //Remove all player in game out of match making.
            ZMatchMaking.Instance.Queue.Remove(allUser[i]);
        }

        //Set room state to be play
        OnRoomChangeState(RoomState.Preparing);
        isRoomReady = true;

        Debug.Log("ZGameRoom: Room with roomID : " + roomID + " ready! Set room state to Playing");
    }

    public void OnRoomChangeState(RoomState state)
    {
        roomState = state;
    }

    //This function call when no user in room
    public void OnNoUserOnRoom()
    {
        //If current room are lower than maximum room allowed, reset it, else close it.
        if (ZMatchMaking.Instance.currentRoom < ZMatchMaking.Instance.maximumRoom)
        {
            Debug.Log("ZGameRoom: Room with roomID : " + roomID +  " has no user on room, choose OnResetRoom");
            OnRoomReset();
        }
        else
        {
            Debug.Log("ZGameRoom: Room with roomID : " + roomID + " has no user on room, choose OnRoomDestroy");
            OnRoomDestroy();
        }
    }

    public void OnRoomReset()
    {
        //Change to full state so that no one can access.
        OnRoomChangeState(RoomState.Full);

        //Close game server relate
        Transfer.TargetReceiveRoomClose(Transfer.connectionToClient);

        //Reset room stats
        currentRoomPlayer = 0;
        roomPassword = null;
        isRoomReady = false;
        isHasPassword = false;               

        //Remove all player as child
        for (int i = 0; i < allUser.Count; i++)
        {
            if (allUser[i] != null)
                allUser[i].transform.parent = ZMasterServer.Instance.transform;
        }

        //Reset all room player
        allUser.Clear();
      
        //Detroys the transfer on both MasterServer and GameServer
        NetworkServer.DestroyPlayerForConnection(Transfer.connectionToClient);
        Transfer = null;

        //Reset current room state to let user can join.
        OnRoomChangeState(RoomState.Empty);
        Debug.Log("ZGameRoom: Room with roomID : " + roomID + " reseted! Set room state to Empty");
    }

    public void OnRoomDestroy()
    {
        //Change to full state so that no one can access.
        OnRoomChangeState(RoomState.Full);

        //Tell the game server disconnect all player then close.
        Transfer.TargetReceiveRoomClose(Transfer.connectionToClient);
        
        //Remove room and destroy base on room type
        switch (roomType)
        {
            case RoomType.Normal:
                ZMatchMaking.Instance.listRoomNormal.Remove(this);
                break;
            case RoomType.Tournament:
                ZMatchMaking.Instance.listRoomRanking.Remove(this);
                break;
        }

        //Add current room id to unused port.
        ZMatchMaking.Instance.unusedPort.Enqueue(roomID);
        ZMatchMaking.Instance.currentRoom--;

        //Detroys the transfer on both MasterServer and GameServer
        NetworkServer.DestroyPlayerForConnection(Transfer.connectionToClient);

        Debug.Log("ZGameRoom: Room with roomID : "+ roomID +" destroyed!");

        //And destroy the room
        Destroy(this.gameObject);
    }
    #endregion

    #region GAME SERVER

    //This function uses for start GameServer.exe automatically.
    public void StartGameRoom()
    {
        //Start GameServer.exe via command line, all argument will pass as parameter
        gameServerCmd = "/C "
        + ZConfig.gameRoomPath + " "
        + ZConfig.argsParam1 + " "
        + ZConfig.argsParam2 + " "
        + roomConnectInfo.IP + " "
        + roomConnectInfo.port.ToString() + " "
        + roomType.ToString() + " "
        + maxRoomPlayer.ToString() + " "
        + roomID.ToString();

        Debug.Log(gameServerCmd);
        System.Diagnostics.Process.Start("cmd.exe", gameServerCmd);
    }

    //This function uses for bypass firewall automatically 
    public void StartOpenGamePort()
    {
        //Start OpenPort
        portForwardCmd = string.Format("/C netsh advfirewall firewall add rule  name={0} dir=in action=allow protocol=any localport={1}", "GameServerID-" + roomID.ToString(), roomConnectInfo.port);
        System.Diagnostics.Process.Start("cmd.exe", portForwardCmd);

        portForwardCmd = string.Format("/C netsh advfirewall firewall add rule  name={0} dir=out action=allow protocol=any localport={1}", "GameServerID-" + roomID.ToString(), roomConnectInfo.port);
        System.Diagnostics.Process.Start("cmd.exe", portForwardCmd);

    }
    #endregion
}
