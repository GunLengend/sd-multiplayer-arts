﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;


//Take care the Database task.
//Load and save SQL and non-sql data from server side.
//Respone any in-game data query

public class ZMatchDatabase : MonoBehaviour
{
    [Header("Settings")]
    public bool useLogInFile;
    public bool isConnectDataSuccess;

    [SerializeField] private ConnectionState stateConnection;
    [SerializeField] private SqlConnection conn;

    #region USER DATA
    //Check login data 
    //Input : username, password
    //Output : Checkcode and UID     -   Checkode = 0 -> Success, Checkode = 1 -> WrongUserOrPass, CheckCode = 2 Account not exits, Checkcode = 3 -> Unknown
    public Dictionary<string, string> DataCheckLogin(string userName, string passWord, string idFacecbook, string idGoogle, string idDevices, ZLogin.LoginType type)
    {
        Dictionary<string, string> checkCode_UID = new Dictionary<string, string>();

        switch (type)
        {
            case ZLogin.LoginType.Normal:
                checkCode_UID = CheckLoginNormal(userName, passWord);
                break;
            case ZLogin.LoginType.Facebook:
                checkCode_UID = CheckLoginFacebook(idFacecbook);
                break;
            case ZLogin.LoginType.Google:
                checkCode_UID = CheckLoginGoogle(idGoogle);
                break;
            case ZLogin.LoginType.Quick:
                checkCode_UID = CheckLoginQuick(idDevices);
                break;
        }
        return checkCode_UID;
    }


    /// <summary>
    /// Load user info 
    /// </summary>
    /// <param name="UID"></param>
    /// <returns></returns>
    public ZUser.BasicInfo DataLoadUserInfoBasic(string UID)
    {
        ZUser.BasicInfo uInfoBasic = new ZUser.BasicInfo
        {
            nickname = "Bao",
        };

        //try
        //{
        //    SqlCommand cmd = new SqlCommand("P_Get_Basic_Info", conn)
        //    {
        //        CommandType = CommandType.StoredProcedure
        //    };

        //    cmd.Parameters.Add(new SqlParameter("@UID", UID));
        //    SqlDataReader rdr = cmd.ExecuteReader(CommandBehavior.SingleRow);

        //    //While still read, assign data
        //    while (rdr.Read())
        //    {
        //        uInfoBasic.gender = rdr["gender"].ToString().Trim();
        //        uInfoBasic.urlAvatar = rdr["urlAvatar"].ToString().Trim();
        //        uInfoBasic.about = rdr["about"].ToString().Trim();
        //        uInfoBasic.nickname = rdr["nickname"].ToString().Trim();
        //        uInfoBasic.statusMessage = rdr["statusMessage"].ToString().Trim();

        //        uInfoBasic.loginType = (ZLogin.LoginType)Enum.Parse(typeof(ZLogin.LoginType), rdr["loginType"].ToString().Trim(), true);
        //    }

        //    rdr.Close();
        //}
        //catch (Exception ex)
        //{
        //    Debug.LogError(ex.ToString());
        //}
        return uInfoBasic;
    }

    /// <summary>
    /// Load user info private
    /// </summary>
    /// <param name="UID"></param>
    /// <returns></returns>
    public ZUser.PrivateInfo DataLoadUserInfoPrivate(string UID)
    {
        ZUser.PrivateInfo userInfoPrivate = new ZUser.PrivateInfo
        {
            address = "Abc",
        };
        
        //try
        //{
        //    SqlCommand cmd = new SqlCommand("P_Get_Private_Info", conn)
        //    {
        //        CommandType = CommandType.StoredProcedure
        //    };
        //    cmd.Parameters.Add(new SqlParameter("@UID", UID));
        //    SqlDataReader rdr = cmd.ExecuteReader(CommandBehavior.SingleRow);

        //    //While still read, assign data
        //    while (rdr.Read())
        //    {
        //        userInfoPrivate.email = rdr["email"].ToString().Trim();
        //        userInfoPrivate.phoneNumber = rdr["phoneNumber"].ToString().Trim();
        //        userInfoPrivate.address = rdr["address"].ToString().Trim();
        //    }

        //    rdr.Close();
        //}
        //catch (Exception ex)
        //{
        //    Debug.LogError(ex.ToString());
        //}

        return userInfoPrivate;
    }
    /// <summary>
    /// Load user game data
    /// </summary>
    /// <param name="UID"></param>
    /// <returns></returns>
    public ZGameInfo.GameInfo DataLoadUserInfoDataGame(string UID)
    {
        ZGameInfo.GameInfo data = new ZGameInfo.GameInfo();
        data.gold = 100;

        //try
        //{
        //    SqlCommand cmd = new SqlCommand("P_Get_Game_Info", conn)
        //    {
        //        CommandType = CommandType.StoredProcedure
        //    };
        //    cmd.Parameters.Add(new SqlParameter("@UID", UID));
        //    SqlDataReader rdr = cmd.ExecuteReader(CommandBehavior.SingleRow);

        //    //While still read, assign data
        //    while (rdr.Read())
        //    {
        //        data.gold = (long)rdr["gold"];
        //        data.cash = (long)rdr["cash"];
        //        data.exp = (long)rdr["exp"];
        //        data.gamePlayed = (int)rdr["countGamePlayed"];
           
        //        data.winCount = (int)rdr["winCount"];
        //        data.loseCount = (int)rdr["loseCount"];
        //    }

        //    rdr.Close();
        //}
        //catch (Exception ex)
        //{
        //    Debug.LogError(ex.ToString());
        //}

        return data;
    }
    #endregion

    #region REGISTRATION AND INITIAL DATA

    //Create registration data
    //Input : username, email, password, login type
    //Progess : Create basic account data, game data, player data
    //Output : Checkcode and UID  -    Checkode = 0 -> Success, Checkode = 1 -> Username exist, Checkcode = 2 -> Email already used, Checkcode = 3 -> Unknown
    public Dictionary<string, string> DataRegistrationAccount(RegistrationMessage dataRegister)
    {
        Dictionary<string, string> checkCode_UID = new Dictionary<string, string>();
        switch (dataRegister.loginType)
        {
            case ZLogin.LoginType.Normal:
                checkCode_UID = RegisterNormal(dataRegister);
                break;
            case ZLogin.LoginType.Facebook:
                checkCode_UID = RegisterFacebook(dataRegister);
                break;
            case ZLogin.LoginType.Google:
                checkCode_UID = RegisterGoogle(dataRegister);
                break;
        }

        return checkCode_UID;
    }

    /// <summary>
    /// Register account Normal, if cretead login success, created basic data user
    /// </summary>
    /// <param name="dataRegister"></param>
    /// <returns></returns>
    private Dictionary<string, string> RegisterNormal(RegistrationMessage dataRegister)
    {
        string _UID = "";
        Dictionary<string, string> result = new Dictionary<string, string>();
        if (dataRegister.username == "" || dataRegister.password == "")
        {
            result.Add("CheckCode", "3");
            result.Add("UID", "data input not valid");
            return result;
        }
        bool isOK = false;
        //Get data
        try
        {
            SqlCommand cmd = new SqlCommand("P_Register_Normal", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@user", dataRegister.username));
            cmd.Parameters.Add(new SqlParameter("@pass", dataRegister.password));
            SqlDataReader rdr = cmd.ExecuteReader(CommandBehavior.SingleRow);
            //While still read, assign data
            while (rdr.Read())
            {
                if (rdr["UID"].ToString() == "0")
                {
                    result.Add("CheckCode", "1");
                    result.Add("UID", "Exits Username");
                }
                else
                {
                    isOK = true;
                    result.Add("CheckCode", "0");
                    result.Add("UID", rdr["UID"].ToString());
                    _UID = rdr["UID"].ToString();
                }
            }
            rdr.Close();
        }
        catch (Exception ex)
        {
            result.Add("CheckCode", "3");
            result.Add("UID", "UnknowError");
            Debug.LogError(ex.ToString());
        }
        if (isOK)
        {
            CreatedDataBasicUser(dataRegister, _UID);

        }
        return result;
    }

    /// <summary>
    /// Register account Facebook, if cretead login success, created basic data user
    /// </summary>
    /// <param name="dataRegister"></param>
    /// <returns></returns>
    public Dictionary<string, string> RegisterFacebook(RegistrationMessage dataRegister)
    {
        Dictionary<string, string> result = new Dictionary<string, string>();
        string _UID = "";
        if (dataRegister.idFacebook == "")
        {
            result.Add("CheckCode", "3");
            result.Add("UID", "data input not valid");
            return result;
        }
        bool isOk = false;
        //Get data
        try
        {
            SqlCommand cmd = new SqlCommand("P_Register_Facebook", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@idFacebook", dataRegister.idFacebook));
            SqlDataReader rdr = cmd.ExecuteReader(CommandBehavior.SingleRow);
            //While still read, assign data
            while (rdr.Read())
            {
                if (rdr["UID"].ToString() == "0")
                {
                    result.Add("CheckCode", "1");
                    result.Add("UID", "Exits IDFacebook");
                }
                else
                {
                    isOk = true;
                    result.Add("CheckCode", "0");
                    result.Add("UID", rdr["UID"].ToString());
                    _UID = rdr["UID"].ToString();
                }
            }
            rdr.Close();
        }
        catch (Exception ex)
        {
            result.Add("CheckCode", "3");
            result.Add("UID", "UnknowError");
            Debug.LogError(ex.ToString());
        }
        if (isOk)
        {
            CreatedDataBasicUser(dataRegister, _UID);
    
        }
        return result;
    }

    /// <summary>
    /// Register account Google, if cretead login success, created basic data user
    /// </summary>
    /// <param name="dataRegister"></param>
    /// <returns></returns>
    private Dictionary<string, string> RegisterGoogle(RegistrationMessage dataRegister)
    {

        Dictionary<string, string> result = new Dictionary<string, string>();
        string _UID = "";
        if (dataRegister.idFacebook == "")
        {
            result.Add("CheckCode", "3");
            result.Add("UID", "data input not valid");
            return result;
        }
        bool isOk = false;
        //Get data
        try
        {
            SqlCommand cmd = new SqlCommand("P_Register_Google", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@idGoogle", dataRegister.username));
            SqlDataReader rdr = cmd.ExecuteReader(CommandBehavior.SingleRow);
            //While still read, assign data
            while (rdr.Read())
            {
                if (rdr["UID"].ToString() == "0")
                {
                    result.Add("CheckCode", "1");
                    result.Add("UID", "Exits idGoogle");
                }
                else
                {
                    isOk = true;
                    result.Add("CheckCode", "0");
                    result.Add("UID", rdr["UID"].ToString());
                    _UID = rdr["UID"].ToString();
                }
            }
            rdr.Close();
        }
        catch (Exception ex)
        {
            result.Add("CheckCode", "3");
            result.Add("UID", "UnknowError");
            Debug.LogError(ex.ToString());
        }
        if (isOk)
        {
            CreatedDataBasicUser(dataRegister, _UID);

        }
        return result;
    }

    public Dictionary<string, string> RegisterQuick(RegistrationMessage dataRegister)
    {

        Dictionary<string, string> result = new Dictionary<string, string>();
        string _UID = "";
        bool isOk = false;

        //Get data
        try
        {
            SqlCommand cmd = new SqlCommand("P_Register_Quick", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@idDevices", dataRegister.idDevice));
            SqlDataReader rdr = cmd.ExecuteReader(CommandBehavior.SingleRow);
            //While still read, assign data
            while (rdr.Read())
            {
                if (rdr["UID"].ToString() == "0")
                {
                    result.Add("CheckCode", "1");
                    result.Add("UID", "Exits idQuick");
                }
                else
                {
                    isOk = true;
                    result.Add("CheckCode", "0");
                    result.Add("UID", rdr["UID"].ToString());
                    _UID = rdr["UID"].ToString();
                }
            }
            rdr.Close();
        }
        catch (Exception ex)
        {
            result.Add("CheckCode", "3");
            result.Add("UID", "UnknowError");
            Debug.LogError(ex.ToString());
        }
        if (isOk)
        {
            CreatedDataBasicUser(dataRegister, _UID);

        }
        return result;
    }

    /// <summary>
    /// Created basic data user in database
    /// </summary>
    /// <param name="dataRegister"></param>
    private void CreatedDataBasicUser(RegistrationMessage dataRegister, string UID)
    {
        try
        {
            SqlCommand cmd = new SqlCommand("P_Created_Basic_Data", conn);
            //(@UID bigint, @name nvarchar(50), @gender nchar(10), @urlAvartar nvarchar(200), @about nvarchar(200),	@email nchar(100), @phoneNumber nchar(20), @address nvarchar(200))
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@UID", UID));
            cmd.Parameters.Add(new SqlParameter("@name", "name"));
            cmd.Parameters.Add(new SqlParameter("@gender", "gender"));
            cmd.Parameters.Add(new SqlParameter("@urlAvartar", "1"));
            cmd.Parameters.Add(new SqlParameter("@about", "about"));
            cmd.Parameters.Add(new SqlParameter("@email", ""));
            cmd.Parameters.Add(new SqlParameter("@phoneNumber", "Phone"));
            cmd.Parameters.Add(new SqlParameter("@address", "address"));
            cmd.Parameters.Add(new SqlParameter("@nickname", "Guest" + UID));
            cmd.ExecuteNonQuery();
        }
        catch (Exception ex)
        {

        }
    }
    #endregion

    #region LOGIN
    /// <summary>
    /// function login normal with username and password
    /// </summary>
    /// <param name="userName"></param>
    /// <param name="passWord"></param>
    /// <returns></returns>
    private Dictionary<string, string> CheckLoginNormal(string userName, string passWord)
    {
        Dictionary<string, string> result = new Dictionary<string, string>();
        bool isOk = false;
        //Get data
        try
        {
            SqlCommand cmd = new SqlCommand("P_Login_Nomal", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@user", userName));
            cmd.Parameters.Add(new SqlParameter("@pass", passWord));
            SqlDataReader rdr = cmd.ExecuteReader(CommandBehavior.SingleRow);

            //While still read, assign data
            while (rdr.Read())
            {
                isOk = true;

                result.Add("CheckCode", "0");
                result.Add("UID", rdr["UID"].ToString());
            }

            rdr.Close();
        }
        catch (Exception ex)
        {
            result.Add("CheckCode", "3");
            Debug.LogError(ex.ToString());
        }

        if (!isOk)
        {
            result.Add("CheckCode", "1");
        }
        return result;
    }

    private Dictionary<string, string> CheckLoginQuick(string idDevices)
    {
        Dictionary<string, string> result = new Dictionary<string, string>();
        bool isOk = false;
        ////Get data
        //try
        //{
        //    SqlCommand cmd = new SqlCommand("P_Login_Quick", conn);
        //    cmd.CommandType = CommandType.StoredProcedure;
        //    cmd.Parameters.Add(new SqlParameter("@idDevices", idDevices));
        //    SqlDataReader rdr = cmd.ExecuteReader(CommandBehavior.SingleRow);

        //    //While still read, assign data
        //    while (rdr.Read())
        //    {
        //        isOk = true;

        //        result.Add("CheckCode", "0");
        //        result.Add("UID", rdr["UID"].ToString());
        //    }

        //    rdr.Close();
        //}
        //catch (Exception ex)
        //{
        //    result.Add("CheckCode", "3");
        //    Debug.LogError(ex.ToString());
        //}

        //if (!isOk)
        //{
        //    result.Add("CheckCode", "2");
        //}

        result.Add("CheckCode", "0");
        result.Add("UID", "1");
        return result;
    }

    /// <summary>
    /// function login facebook with Google id
    /// </summary>
    /// <param name="idFacebook"></param>
    /// <returns></returns>
    private Dictionary<string, string> CheckLoginGoogle(string idGoogle)
    {
        Dictionary<string, string> result = new Dictionary<string, string>();
        if (idGoogle == "")
        {
            result.Add("CheckCode", "1");
            result.Add("UID", "");
            return result;
        }
        bool isOk = false;
        //Get data
        try
        {
            SqlCommand cmd = new SqlCommand("P_Login_Google", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@idGoogle", idGoogle));
            SqlDataReader rdr = cmd.ExecuteReader(CommandBehavior.SingleRow);

            //While still read, assign data
            while (rdr.Read())
            {
                isOk = true;

                result.Add("CheckCode", "0");
                result.Add("UID", rdr["UID"].ToString());
            }

            rdr.Close();
        }
        catch (Exception ex)
        {
            result.Add("CheckCode", "3");
            Debug.LogError(ex.ToString());
        }

        if (!isOk)
        {
            result.Add("CheckCode", "1");
        }
        return result;
    }

    /// <summary>
    /// function login facebook with facebook id
    /// </summary>
    /// <param name="idFacebook"></param>
    /// <returns></returns>
    private Dictionary<string, string> CheckLoginFacebook(string idFacebook)
    {
        Dictionary<string, string> result = new Dictionary<string, string>();
        if (idFacebook == "")
        {
            result.Add("CheckCode", "1");
            result.Add("UID", "");
            return result;
        }
        bool isOk = false;
        //Get data
        try
        {
            SqlCommand cmd = new SqlCommand("P_Login_Facebook", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@idFacebook", idFacebook));
            SqlDataReader rdr = cmd.ExecuteReader(CommandBehavior.SingleRow);

            //While still read, assign data
            while (rdr.Read())
            {
                isOk = true;

                result.Add("CheckCode", "0");
                result.Add("UID", rdr["UID"].ToString());
            }

            rdr.Close();
        }
        catch (Exception ex)
        {
            result.Add("CheckCode", "3");
            Debug.LogError(ex.ToString());
        }

        if (!isOk)
        {
            result.Add("CheckCode", "2");
        }
        return result;
    }


    public void UpdateStatusLogin(string UID, bool status)
    {
        try
        {
            SqlCommand cmd = new SqlCommand("P_Update_Status_Login", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@UID", UID));
            cmd.Parameters.Add(new SqlParameter("@status", status));
            cmd.ExecuteNonQuery();
        }
        catch (Exception ex)
        {
            Debug.LogError(ex.ToString());
        }
    }
    #endregion

}


