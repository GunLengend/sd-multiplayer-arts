﻿using System.Collections.Generic;
using UnityEngine;

public class ZUserManager : MonoBehaviour
{
    public List<ZUser> usersOnline;
    public List<ZUser> disconnectedUsers = new List<ZUser>();
    public uint onlineUser;

    public void AddUserOnline(ZUser user)
    {
        if (user == null)
            return;

        usersOnline.Add(user);
        onlineUser++;

        Debug.Log("NUserManager: Add user " + user.basicInfo.nickname + " to list online");

        this.PostEvent(ZEventId.OnUserOnline, user);
    }

    public void RemoveUserOffline (ZUser user)
    {
        if (user == null)
            return;

        usersOnline.Remove(user);
        onlineUser--;

        Debug.Log("NUserManager: Remove user " + user.basicInfo.nickname + " out of list online");
        //ZMasterServer.Instance.Data.UpdateStatusLogin(user.UID, false);

        this.PostEvent(ZEventId.OnUserOffline, user);
    }

    public bool IsUserOnline(string userId)
    {
        return usersOnline.Exists(x => x.basicInfo.AID == userId);
    }

    public ZUser GetUserOnline(string userId)
    {
        if (IsUserOnline(userId))
            return usersOnline.Find(x => x.basicInfo.AID == userId);
        else
            return null;
    }
}
