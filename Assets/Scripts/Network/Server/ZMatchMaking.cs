﻿#define ENABLE_MATCH_MAKING_MODE

#if ENABLE_MATCH_MAKING_MODE
using UnityEngine;
using System.Collections.Generic;

/// <summary>
/// Enable by using ENABLE_MATCH_MAKING_MODE. This script can attach to any 
/// game object on the scene and it will help player can 
/// find match and join room to communication with each other. 
/// This system include both rank match and normal match by linear search queue.
/// </summary>

public class ZMatchMaking : MonoBehaviour 
{
    public List<ZUser> Queue;                                               //List user in queue
    public Queue<int> unusedPort;                                           //List port unused

    public int defaultRoom;                                                 //The quantity of default room will create automatically when start.
    public int maximumRoom;                                                 //The maximum room and GameServer allowed to open on dedicated server
    public int currentRoom;                                                 //The current room and GameServer are opened.
    public bool enableBotMode;                                              //Is enable bot mode? Player will also play with bot.
    public GameObject GameRoom;                                             //The GameRoom reference.

    //List room created
    public List<ZGameRoom> listRoomNormal;                                  //List room normal 
    public List<ZGameRoom> listRoomRanking;                                 //List room ranking

    [HideInInspector] public ZMasterServer Server;                          //Reference of MasterServer.

    //Singleton pattern initial
    private static ZMatchMaking instance;
    public static ZMatchMaking Instance { get { return instance; } }

    #region INITIAL AND CONFIGURATION
    // Use this for initialization
    void Start () 
    {
        //Initializing
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(this);

        maximumRoom = ZConfig.maximumRoomAllowed;
        defaultRoom = ZConfig.defaultRoomQuanity;

        //Reference
        Queue = new List<ZUser>();
        unusedPort = new Queue<int>();
		Server = FindObjectOfType<ZMasterServer>();


        //Auto create room configuration.
        if (ZConfig.ENABLE_AUTO_CREATE_DEFAULT_ROOM)
        {
            foreach(ZGameRoom.Channel channel in System.Enum.GetValues(typeof(ZGameRoom.Channel)))
            {
                
            }
        }
    }
	#endregion

    #region DEFAULT GENERATE
    //This function using for create default room, for development purpose.
    public void CreateDefaultRoom(int roomQuantity, int roomMinBet, ZGameRoom.Channel channel)
    {
        //Define struct created room default 
        FindMatchInfo matchInfo = new FindMatchInfo
        {
            roomType = ZGameRoom.RoomType.Normal,
        };

        //Create default room
        for (int i = 0; i < roomQuantity; i++)
        {
            //Create number of default room
            OnServerCreateRoom(matchInfo, null);

            //Create number of vip room by change the type
            matchInfo.roomType = ZGameRoom.RoomType.Tournament;
            OnServerCreateRoom(matchInfo, null);
        }

        Debug.Log(string.Format("ZMatchMaking: MatchMaking create {0} default room", roomQuantity));
    }
    #endregion

    #region MATCH MAKING
    //This function will add player to Queue and call find match with match info
    public void AddPlayerToQueue(ZUser player, FindMatchInfo matchInfo)
    {
        //Add player to MatchMaking Queue
        Queue.Add(player);

        //Find match
        MatchMaking(player, matchInfo);
    }

    //This function will find match following match making condition and match info
    public void MatchMaking(ZUser player, FindMatchInfo matchInfo)
    {
        switch (matchInfo.roomType)
        {
            case ZGameRoom.RoomType.Normal:
                //If at least one room exist in list room
                if (listRoomNormal.Count != 0)
                {
                    //Search room
                    bool founded = false;

                    if (!founded)
                    {
                        for (int i = 0; i < listRoomNormal.Count; i++)
                        {
                            Debug.Log("MatchMaking.cs : Search at room " + i + " of " +listRoomNormal.Count + " room");

                            //If room are waiting and matching with roomType, minBet, minChip allowed, no password and do not contain same player.
                            if (listRoomNormal[i].roomState != ZGameRoom.RoomState.Full )
                            {
                                //Founded !
                                Debug.Log("MatchMaking.cs : Room founded ! Room index : " + i);
                                founded = true;

                                //Let player join room
                                listRoomNormal[i].OnPlayerJoinRoom(player);

                                //Stop searching
                                return;
                            }
                            //Else that mean there is no room available.
                            else if (i == listRoomNormal.Count - 1)
                            {
                                Debug.Log("MatchMaking.cs : Create room cause all room full!");

                                //Create new room.
                                OnServerCreateRoom(matchInfo, player);
                                founded = true;
                                return;
                            }
                        }
                    }
                }
                else
                {
                    //Create new room cause no room was exist!
                   OnServerCreateRoom(matchInfo, player);
                    Debug.Log("MatchMaking.cs : Request create room cause no room exist!");
                }
                break;
            case ZGameRoom.RoomType.Tournament:
                if (listRoomRanking.Count != 0)
                {
                    //Search room
                    bool founded = false;

                    if (!founded)
                    {
                        for (int i = 0; i < listRoomRanking.Count; i++)
                        {
                            Debug.Log("MatchMaking.cs : Search room at room " + i + " of " + listRoomRanking.Count);
                            //If room not full, even playing or waiting with roomType, minBet
                            if (listRoomRanking[i].roomState != ZGameRoom.RoomState.Full)
                            {
                                //Let player join room
                                listRoomRanking[i].OnPlayerJoinRoom(player);

                                //Stop searching
                                Debug.Log("MatchMaking.cs : Room founded ! Room index : " + i);

                                founded = true;
                                return;
                            }
                            else if (i == listRoomRanking.Count - 1)
                            {
                                Debug.Log("MatchMaking.cs : Create room cause all room full!");

                                OnServerCreateRoom(matchInfo, player);
                                founded = true;
                                return;
                            }
                        }
                    }
                }
                else
                {
                    Debug.Log("MatchMaking.cs : Request create room cause no room exist!");

                    //Create new room
                   OnServerCreateRoom(matchInfo, player);
                }
                break;
        }
    }
    #endregion

    #region MATCH MAKING ROOM CONTROLLER
    //Server create room, this usually call by match making
    public void OnServerCreateRoom(FindMatchInfo matchInfo, ZUser user)
    {
        if (currentRoom >= maximumRoom)
        {
            user.TargetReceiveCannotCreateRoom(ZString.POPUP_MAXIMUN_ROOM_ALLOWED);
            return;
        }

        //Creat room and assign room default value.
        ZGameRoom room = Instantiate<GameObject>(GameRoom).GetComponent<ZGameRoom>();
        room.roomType = matchInfo.roomType;
        room.roomPassword = null;
        room.maxRoomPlayer = ZConfig.defaultMaxRoomPlayer;
        room.minRoomPlayer = ZConfig.defaultMinRoomPlayer;

        room.transform.parent = ZMatchMaking.Instance.transform;

        //If unsused port list equal 0, that mean not any room closed.
        if (unusedPort.Count == 0)
            room.roomID = listRoomNormal.Count + listRoomRanking.Count + 1;
        //Else that mean we have at least one room closed, used that id instead.
        else
            room.roomID = unusedPort.Dequeue();

        room.name = "Room " + room.roomID.ToString();

        //Add room created to list room based on type.
        switch (matchInfo.roomType)
        {
            case ZGameRoom.RoomType.Normal:
                listRoomNormal.Add(room);
                break;
            case ZGameRoom.RoomType.Tournament:
                listRoomRanking.Add(room);
                break;
        }

        //Then let the room get connect info based on the config.
        room.roomConnectInfo.IP = ZConfig.masterServerIP;
        room.roomConnectInfo.port = ZConfig.minGameRoomPort + room.roomID;

        //Finally, count the current room are opened
        currentRoom++;

        //If room was created by player.
        if (user != null)
        {
            //Let that user assign to room player.
            room.OnRoomCreate(user);
        }
    }

    //User create room, it's usually call by user create command
    public void OnUserCreateRoom(CreateMatchInfo matchInfo, ZUser user)
    {
        if (currentRoom >= maximumRoom)
        {
            user.TargetReceiveCannotCreateRoom(ZString.POPUP_MAXIMUN_ROOM_ALLOWED);
            return;
        }

        //Creat room and assign room default value.
        ZGameRoom room = Instantiate<GameObject>(GameRoom).GetComponent<ZGameRoom>();
        room.roomType = matchInfo.roomType;

        room.roomPassword = matchInfo.roomPassword;
        room.maxRoomPlayer = matchInfo.numberPlayer + 2;
        room.minRoomPlayer = ZConfig.defaultMinRoomPlayer;
        room.transform.parent = this.transform;

        //If unsused port list equal 0, that mean not any room closed.
        if (unusedPort.Count == 0)
            room.roomID = listRoomNormal.Count + listRoomRanking.Count + 1;
        //Else that mean we have at least one room closed, used that id instead.
        else
            room.roomID = unusedPort.Dequeue();

        room.name = "Room " + room.roomID.ToString();

        //Add room created to list room based on type.
        switch (matchInfo.roomType)
        {
            case ZGameRoom.RoomType.Normal:
                listRoomNormal.Add(room);
                break;
            case ZGameRoom.RoomType.Tournament:
                listRoomRanking.Add(room);
                break;
        }

        //Then let the room get connect info based on the config.
        room.roomConnectInfo.IP = ZConfig.masterServerIP;
        room.roomConnectInfo.port = ZConfig.minGameRoomPort + room.roomID;

        //Finally, count current room are opened
        currentRoom++;

        //If room was created by player.
        if (user != null)
        {
            //Let that user assign to room player.
            room.OnRoomCreate(user);
        }
    }

    //Server detele room
    public void OnServerDeteleRoom(int roomID)
    {
        //TODO: Find roomId in list room, then destroy it.
        //TODO: This using for Admin command, another call 
        //OnRoomDestroy in ZGameRoom
    }
    #endregion
}
#endif
