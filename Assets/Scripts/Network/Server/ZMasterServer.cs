﻿using UnityEngine;
using Mirror;
using Mirror.Authenticators;

/// <summary>
/// This script can be attached to any game object on the scene and it
/// will let the ZMasterClient can connect via listen port and publish IP
/// </summary>

public class ZMasterServer : MonoBehaviour 
{
    public enum ServerStatus
    {
        Offline,
        Online,
        Full,
        Maintenance
    }

    //Server information
    [SerializeField] private int listenPort;
    [SerializeField] private bool enableNAT;
    [SerializeField] private Transport transport;

    //Server status
    public float serverTime;
	public uint currentConnection;
	public int maxConnection;
    public bool initialOk;
    public ServerStatus serverStatus;

    //Reference
    public ZGlobalAsset Asset;
    public ZMatchDatabase Data;
    public ZUserManager UserManager;

    //Singleton pattern initial
    private static ZMasterServer instance;
    public static ZMasterServer Instance { get { return instance; } }

	#region REFERENCE AND CONFIGURATION
    void Awake()
    {
        //Reference finder
        if (Asset == null)
            Asset = GameObject.FindGameObjectWithTag("GlobalAsset").GetComponent<ZGlobalAsset>();

        DontDestroyOnLoad(this);

        //This prevent instance double when load new scene or back to current scene, for develop only
        if (instance == null) 
        {
            instance = this;
        } 
        else 
        {
            Destroy(gameObject);
        }
    }

	// Use this for initialization
	void Start () 
    {
        Debug.Log("ZMasterServer: Server initializing...");

        //Figure initialize
        maxConnection = ZConfig.maxConnection;

        //System handle
        NetworkServer.OnConnectedEvent = OnClientConnected;
        NetworkServer.OnDisconnectedEvent = OnClientDisconnected;
        NetworkServer.RegisterHandler<ObjectAddTypeMessage>(OnServerAddPlayer, false);

        NetworkServer.RegisterHandler<ReconnectMessage>(OnClientReconnected, false);
        NetworkServer.RegisterHandler<StringMessage>(OnClientLogOut, false);

        //Transport initial
        Transport.activeTransport = transport;
        transport.SetServerPort(ZConfig.masterServerPort);

        //Listening...
        NetworkServer.Listen(maxConnection);

        //Set status
        initialOk = true;
        serverStatus = ServerStatus.Online;

        Debug.Log("ZMasterServer: Server initialized");
    }
    #endregion

    #region PLAYER CONNECTION HANDLER
    //Client connected to server
    void OnClientConnected(NetworkConnection con)
    {
        currentConnection++;
        Debug.Log("ZMasterServer: A client connected, connection id: " + con.connectionId);
    }

    //Client disconnected to server
    void OnClientDisconnected(NetworkConnection con)
    {
        //Subtract connections to server 
        if (currentConnection > 0)
        {
            currentConnection--;
            Debug.Log("ZMasterServer: A client disconnected, connection id: " + con.connectionId);
        }

        if (con.identity == null)
            return;

        //Find disconnect user.
        ZUser disconnectedUser = con.identity.gameObject.GetComponent<ZUser>();
        if (disconnectedUser != null)
        {
            disconnectedUser.isDisconnected = true;
            UserManager.disconnectedUsers.Add(disconnectedUser);
            Debug.Log("ZMasterServer: Find out the player disconnected object, add to list disconnected");
        }
        else
            return;

        //Remove it out of list online to let user can login again.
        UserManager.RemoveUserOffline(disconnectedUser);

        //If it still in the room 
        if (disconnectedUser.isJoinedRoom)
        {
            //Start count disconnect time out 
            disconnectedUser.DisconnectTimeout(ZConfig.disconnectTimeOutPlaying);

            Debug.Log("ZMasterServer : User " + disconnectedUser.basicInfo.nickname + " is disconnected but still in room, remove online and start disconnectTimeOut");

            //Get the room that player is joined
            var Room = disconnectedUser.GetComponentInParent<ZGameRoom>();

            //Tell data transfer check this player disconnect in game server or not.
            Room.Transfer.TargetCheckPlayerConnection(Room.Transfer.connectionToClient, disconnectedUser.playerToken);
        }
        //Else remove player out of room
        else
        {
            Debug.Log("ZMasterServer : User " + disconnectedUser.basicInfo.nickname + " is disconnected and not in room");

            //Start count disconnect time out, we don't destroy immediately 
            disconnectedUser.DisconnectTimeout(ZConfig.disconnectTimeOutWaiting);
        }
    }

    public void OnClientReconnected(NetworkConnection con, ReconnectMessage netMsg)
    {
        var assetId = netMsg.assetId;
        ZUser user = UserManager.disconnectedUsers.Find(x => x.UID == netMsg.syncCondition);

        //If user not found, that mean it already destroy by disconnect time out
        if (user == null)
        {
            //Then tell the client re-login again
            StringMessage stringMessage = new StringMessage
            {
                stringValue = "Relog"
            };

            NetworkServer.connections[con.connectionId].Send(stringMessage);
            return;
        }
        else
        {
            //Set is connected and remove out of disconnected list.
            user.isDisconnected = false;
            UserManager.disconnectedUsers.Remove(user);

            //Rematch the user object
            NetworkServer.ReplacePlayerForConnection(con, user.gameObject, assetId);
            Debug.Log("ZGameServer: The user with UID " + user.playerToken + " is reconnected!");
        }
    }

    public void OnClientLogOut(NetworkConnection con, StringMessage netMsg)
    {
        var offlineUser = UserManager.GetUserOnline(netMsg.stringValue);
        UserManager.RemoveUserOffline(offlineUser);

        NetworkServer.DestroyPlayerForConnection(con);
    }

    //Server spawn player
    void OnServerAddPlayer(NetworkConnection con, ObjectAddTypeMessage objAddTypeMsg)
    {
#if ENABLE_MATCH_MAKING_MODE
        switch (objAddTypeMsg.objType)
        {
            case ObjectAddTypeMessage.ObjType.Player:
                GameObject userPrefabs = ZGlobalAsset.Instance.userAsset;
                GameObject user = Instantiate(userPrefabs) as GameObject;
                user.transform.parent = this.transform;

                //Assign UID to player
                user.GetComponent<ZUser>().UID = objAddTypeMsg.strIncluded;

                //Spawn player on client
                NetworkServer.AddPlayerForConnection(con, user);
                break;

            case ObjectAddTypeMessage.ObjType.Transfer:
                GameObject dataPrefabs = ZGlobalAsset.Instance.dataTransAsset;
                GameObject dataTransfer = Instantiate(dataPrefabs) as GameObject;
                dataTransfer.transform.parent = this.transform;

                //Spawn data transfer on Game Server
                NetworkServer.AddPlayerForConnection(con, dataTransfer);

                Debug.Log("ZMasterServer: Add data transfer on Server");
                break;
        }
#endif
    }
    #endregion
}
