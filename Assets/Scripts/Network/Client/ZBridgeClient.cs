﻿using UnityEngine;
using Mirror;
using System;

/// <summary>
/// This script using to create bridge connection between GameServer and Lobby/Master Server.
/// The GameServer via this script act like client of MasterServer, and handle spawn DataTransfer
/// to establish connection and transfer data between each process, in local physical server, this
/// handle for inter-communication process.
/// </summary>

public class ZBridgeClient : MonoBehaviour
{
    [Header("Master Server Info")]
    public string masterServerIP;
    public int masterServerPort;
    public ZDataTransfer Transfer;
    public Transport transport;

    private GameObject dataTransfer;

    private void Awake()
    {
        masterServerIP = "127.0.0.1";
        masterServerPort = ZConfig.masterServerPort;
    }

    // Start is called before the first frame update
    void Start()
    {
        Initialized();
    }

    private void Initialized()
    {
        NetworkClient.OnConnectedEvent = OnConnectedMaster;
        NetworkClient.OnDisconnectedEvent = OnDisconnectedMaster;
        transport.OnClientDataReceived = OnDataReceived;

        Transport.activeTransport = transport;
        transport.SetClientPort((ushort)masterServerPort);
        NetworkClient.Connect(masterServerIP);

        Debug.Log("ZBridgeClient: Bridge client inititalized");
    }

    #region CONNECTION HANDLER

    //This function call when Game Server connect to Master Server
    void OnConnectedMaster()
    {
        Debug.Log("ZGameServer: Connected to master");

        //Assign data transfer object from GlobalAsset
        dataTransfer = ZGlobalAsset.Instance.dataTransAsset;

        //Register data transfer object on this Game Server
        NetworkClient.RegisterPrefab(dataTransfer, OnSpawnDataTransfer, OnDespawnDataTransfer);

        if (!NetworkClient.ready)
            NetworkClient.Ready();

        //Create custom message
        ObjectAddTypeMessage msg = new ObjectAddTypeMessage
        {
            objType = ObjectAddTypeMessage.ObjType.Transfer,
            strIncluded = ZGameServer.Instance.roomID.ToString()
        };

        NetworkClient.Send(msg, 2);
    }

    //This function call when Game Server disconnect to Master Server
    public void OnDisconnectedMaster()
    {
        Debug.Log("ZGameServer: Disconnected to master at matchId: ");
    }

    public void OnDataReceived(ArraySegment<byte> data, int channelId)
    {
        Debug.Log("ZGameServer: Receive data from master");
    }
    #endregion

    #region SPAWN HANDLER
    //This is spawn handler for DataTransfer
    GameObject OnSpawnDataTransfer(Vector3 position, System.Guid assetId)
    {
        //Spawn data transfer prefab
        GameObject dataObject = Instantiate(dataTransfer, dataTransfer.transform.position, Quaternion.identity) as GameObject;

        //Assign player
        Transfer = dataObject.GetComponent<ZDataTransfer>();
        ZGameServer.Instance.Transfer = Transfer;
        Transfer.transform.parent = this.transform;

        //Assign and call sync with master Server
        Transfer.AssignMatchInfo(ZGameServer.Instance.roomID.ToString(), ZGameServer.Instance.roomConnectInfo, ZGameServer.Instance.roomType);

        Debug.Log("ZBridgeClient: Data Transfer spawned");

        //Return player game object
        return dataObject;
    }

    //This is despawn handler for DataTransfer
    void OnDespawnDataTransfer(GameObject spawned)
    {
        Destroy(spawned);
    }
    #endregion
}
