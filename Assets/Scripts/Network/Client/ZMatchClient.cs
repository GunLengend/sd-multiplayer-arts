﻿using System.Collections;
using Mirror;
using UnityEngine;

public class ZMatchClient : MonoBehaviour
{
    [Header("Config")]
    public string gameServerIP;
    public int gameServerPort;
    public int maxPlayerConnect;
    public int reconnectAttempt;
    public bool isDisconnected;

    [Header("Status")]
    public bool isLeaveRoom;
    public bool isSimulate;

    private ZPlayer spawnedPlayer;   
    public GameObject playerPrefabs;
    public Transport transport;

    //Singleton pattern initial
    private static ZMatchClient instance;
    public static ZMatchClient Instance { get { return instance; } }

    #region INITIAL
    void Awake()
    {
        if (instance == null)
            instance = this;
        else
            Destroy(gameObject);
    }

    void Start()
    {
        //Connect to Game Server when scene loading
        ConnectToGameServer(ZGame.Instance.matchInfo);
    }

    public void ConnectToGameServer(MatchInfo match)
    {
        //Assign connection info
        gameServerIP = match.roomInfo.IP;
        gameServerPort = match.roomInfo.port;
        maxPlayerConnect = match.maxPlayerConnect;

        //Register connection handlee
        NetworkClient.OnConnectedEvent = OnClientConnected;
        NetworkClient.OnDisconnectedEvent = OnClientDisconnected;

        //Set transport port
        transport.SetClientPort((ushort)gameServerPort);
        Transport.activeTransport = transport;

        //Connect to GameServer
        NetworkClient.Connect(gameServerIP);

        Debug.Log("ZMatchClient: Client connecting to game server....");
    }
    #endregion

    #region CONNECTION HANDLER
    void OnClientConnected()
    {
        Debug.Log("ZMatchClient: Client connected to game server");

        //Only spawn player in the first time
        if(!isDisconnected)
        {
            //Call spawn player
            OnGameSpawnPlayer(NetworkClient.connection);
        }

        isLeaveRoom = false;
        
        //Set disconnected to false
        isDisconnected = false;
    }

    void OnClientDisconnected()
    {
        Debug.Log("ZMatchClient: Client disconnected to game server");
        isDisconnected = true;

        //If player are playing
        if (!isLeaveRoom)
        {
            //Try to reconnect to server.
            Reconnect();

            // Show loading if reconnect
        }

        //If player are trying to connecting in the first time
        if (!isLeaveRoom && ZGame.Instance.Player == null)
        {
            Reset();
        }
    }
    #endregion

    #region RECONNECT HANDLER
    public void Reset()
    {
        Debug.Log("ZMacthClient: Reset connection...");
        NetworkClient.Shutdown();

        MatchInfo match = new MatchInfo
        {
            maxPlayerConnect = maxPlayerConnect
        };

        match.roomInfo.IP = gameServerIP;
        match.roomInfo.port = gameServerPort;

        ConnectToGameServer(match);
    }

    //This function call when client disconnect or request to reconnect to GameServer.
    public void Reconnect()
    {
        //Reconnecting...
        if (reconnectAttempt <= ZConfig.defaultConnectionAttempt)
        {
            NetworkClient.Connect(gameServerIP);

            reconnectAttempt++;

            //Try to sync both Player game object.
            StartCoroutine(RematchPlayerObject());
        }
        else
        {
            this.PostEvent(ZEventId.OnUserGetConnectionTimeout);
            Debug.Log("ZMatchClient: Cannot connect to game server after " + ZConfig.defaultConnectionAttempt + " time");
        }
    }

    //This function call when is reconnected
    public IEnumerator RematchPlayerObject()
    {
        //Wait until connection established
        yield return new WaitUntil(() => isDisconnected == false);

        reconnectAttempt = 0;

        //Create reconnect message.
        ReconnectMessage msg = new ReconnectMessage
        {
            netId = ZGame.Instance.Player.netId,
            assetId = ZGame.Instance.Player.netIdentity.assetId,
            syncCondition = ZGame.Instance.Player.playerToken
        };

        //Send to Server to re-match object
        NetworkClient.Send(msg);

        //Set client ready to send command/rpc again.
        if (!NetworkClient.ready)
            NetworkClient.Ready();
    }

    //This function using for close connection and disconnect
    public void Disconnect()
    {
        NetworkClient.Disconnect();
        NetworkClient.Shutdown();
    }

    #endregion

    #region PLAYER SPAWN HANDLER
    private void OnGameSpawnPlayer(NetworkConnection con)
    {
        playerPrefabs = ZGlobalAsset.Instance.playerAsset;

        //Register user prefab with spawning system
        NetworkClient.RegisterPrefab(playerPrefabs, OnSpawnPlayer, OnDespawnPlayer);

        if (!NetworkClient.ready)
        {
            NetworkClient.Ready();
        }

        //Call add player to Game Server
        ObjectAddTypeMessage objectAdd = new ObjectAddTypeMessage
        {
            objType = ObjectAddTypeMessage.ObjType.Player,
            strIncluded = "",
        };

        NetworkClient.Send(objectAdd);

        Debug.Log("ZMatchClient: Client call spawning player");
    }

    GameObject OnSpawnPlayer(Vector3 position, System.Guid assetID)
    {
        //Spawn player prefab
        GameObject player = Instantiate(playerPrefabs, playerPrefabs.transform.position, Quaternion.identity) as GameObject;

        //Assign player
        spawnedPlayer = player.GetComponent<ZPlayer>();
        spawnedPlayer.transform.parent = this.transform;

        Debug.Log("Client spawned player!!");

        //Return user game object
        return player;
    }

    void OnDespawnPlayer(GameObject spawned)
    {
        // Remove player 
        Destroy(spawned);
    }
    #endregion
}
