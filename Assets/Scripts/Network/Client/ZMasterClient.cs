﻿using UnityEngine;
using Mirror;
using System.Collections;
using System;

/// <summary>
/// This script using for initial client connection and need to attached to MasterClient game object.
/// The global access via Instance reference and it is singleton object. Loading new scene with connect
/// to new server required to destroy m_Client.
/// </summary>

public class ZMasterClient : MonoBehaviour 
{
    //Server information
    [SerializeField]private string serverURL;
    [SerializeField]private string serverIP;
    [SerializeField]private int serverPort;
    [SerializeField]private int reconnectAttempt;
    [SerializeField]private Transport transport;

    //Client state
    public bool isConnected;

	//Module reference
	public ZLogin Login;
    public ZRegistration Register;

    //Singleton pattern initial
    private static ZMasterClient instance;
    public static ZMasterClient Instance { get { return instance; } }

	#region INITIAL AND CONNECT

    //Set reference to singleton when this Client object create
    //For handle connection, Client object should destroy on load any scenes game
    void Awake()
    {
        Login = GetComponent<ZLogin>();
        Register = GetComponent<ZRegistration>();
        
        //This prevent instance double when load new scene or back to current scene
        if (instance == null) 
            instance = this;
        else
            Destroy(gameObject);

        Initialize();
    }

    void Initialize()
    {
        //System handle
        NetworkClient.OnConnectedEvent = ClientConnected;
        NetworkClient.OnDisconnectedEvent = ClientDisconnected;
        NetworkClient.OnErrorEvent = ClientConnectTimeOut;

        //Post event that client reconnected, to register all message again.
        this.PostEvent(ZEventId.OnClientReconnection);

        //Custom handle
        NetworkClient.RegisterHandler<StringMessage>(ClientReceiveStringMessage);

        //Transport initial
        transport.SetClientPort(ZConfig.masterServerPort);
        Transport.activeTransport = transport;

        //Connecting...
        NetworkClient.Connect(serverIP);

        Debug.Log("ZMatchClient: Initialized! ");
    }

    #endregion

    #region CUSTOM MESSAGE & CONNECTION HANDLER

    //Client receive string message
    void ClientReceiveStringMessage(StringMessage netMsg)
    {
        if (netMsg.stringValue == "Relog")
        {
            Debug.Log("ZMasterClient: Client receive string message: " + netMsg.stringValue);

            if (ZGame.Instance.User != null)
            {
                Destroy(ZGame.Instance.User.gameObject);
                ZGame.Instance.User = null;
                ZGame.Instance.OnGameRespawnUser(ZGame.Instance.localUserID);
            }
        }
    }

    //Handle when client connected
    void ClientConnected()
    {
        Debug.Log("ZMasterClient: Client connected, connection id: " + NetworkClient.connection.connectionId);
        isConnected = true;
        reconnectAttempt = 0;
    }

    void ClientConnectTimeOut(Exception exception)
    {
        Debug.Log("ZMasterClient: Client got error connection, error: " + exception.ToString());
    }

    //Handle when client disconnected
    void ClientDisconnected()
    {
        Debug.Log("ZMasterClient: Client disconnected, connection id: " + NetworkClient.connection.connectionId);
        isConnected = false;

        if (ZGame.Instance.User != null)
            ClientReconnect();
        else
            Reset();
    }

    //Handle when client reconnect
    void ClientReconnect()
    {
        if (reconnectAttempt <= ZConfig.defaultConnectionAttempt)
        {
            NetworkClient.Connect(serverIP);
            reconnectAttempt++;
            StartCoroutine(RematchPlayerObject());
        }
        else
        {
            this.PostEvent(ZEventId.OnUserGetConnectionTimeout);
            Debug.Log("ZMatchClient: Cannot connect to master server after " + ZConfig.defaultConnectionAttempt + " time");
        }
    }

    public IEnumerator RematchPlayerObject()
    {
        yield return new WaitUntil(() => isConnected);
        reconnectAttempt = 0;

        ReconnectMessage msg = new ReconnectMessage
        {
            netId = ZGame.Instance.User.netId,
            assetId = ZGame.Instance.User.netIdentity.assetId,
            syncCondition = ZGame.Instance.localUserID
        };

        NetworkClient.Send(msg);

        if (!NetworkClient.ready)
            NetworkClient.Ready();
    }

    public void Reset()
    {
        if (reconnectAttempt <= ZConfig.defaultConnectionAttempt)
        {
            Debug.Log("ZMasterClient: Client disconnected! Reset connection...");
            reconnectAttempt++;

            Disconnect();
            Initialize();
        }
        else
        {
            this.PostEvent(ZEventId.OnUserGetConnectionTimeout);
            Debug.Log("ZMatchClient: Cannot connect to master server after " + ZConfig.defaultConnectionAttempt + " time");
        }
    }

    //This function using for close connection and disconnect
    public void Disconnect()
    {
        NetworkClient.Disconnect();
        NetworkClient.Shutdown();
    }
    #endregion
}
