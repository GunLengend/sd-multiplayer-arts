using Mirror;
using System.Collections.Generic;

public class ZDataVisibility : NetworkVisibility
{   
    //This function call when new connection connect to Master Server
    public override bool OnCheckObserver(NetworkConnection newObserver)
    {
        return false;
    }

    //This function call when force rebuild all observe list, current call on Master as Start()
    public override void OnRebuildObservers(HashSet<NetworkConnection> observers, bool initial)
    {
        observers.Clear();
        observers.Add(this.connectionToClient);
    }

    public override void OnDeserialize(NetworkReader reader, bool initialState)
    {
        base.OnDeserialize(reader, initialState);
     }
}
