﻿using UnityEngine;
using Mirror;
using System;

/// <summary>
/// "User" is different from "Player". This using for
/// communication and sync account data and only available 
/// between Game Server and Master Server. You can only own 
/// one user per account, but can owned more than one player 
/// per user
/// </summary>

public class ZUser : NetworkBehaviour
{
    [System.Serializable]
    public enum UpdateInfoType
    {
        Nickname,
        StatusMessage,
        Password,
        Avatar,
        Email,
    }

    [System.Serializable]
    public struct BasicInfo
    {
        public string gender;
        public string urlAvatar;
        public string about;
        public string AID;
        public string nickname;
        public bool status;
        public string statusMessage;
        public ZLogin.LoginType loginType;
    }
  
    [System.Serializable]
    public struct PrivateInfo
    {
        public string email;
        public string phoneNumber;
        public string address;
    }

    public string UID;
    public string playerToken;
    public BasicInfo basicInfo;
    public PrivateInfo privateInfo;

    public bool isJoinedRoom;
    public bool isDisconnected;

    //Reference
    [HideInInspector] public ZMatchFinding Match;
    [HideInInspector] public ZGameInfo GameInfo;


    #region INITIAL AND REFERENCE
    // Use this for initialization
    private void Awake()
    {
        //Global reference, this will run both client and server
        Match = GetComponent<ZMatchFinding>();
        GameInfo = GetComponent<ZGameInfo>();
    }

    public override void OnStartLocalPlayer()
    {
        base.OnStartLocalPlayer();

        //Assign to ZGame if is local player
        ZGame.Instance.User = this;
    }

    public override void OnStartServer()
    {
        base.OnStartServer();

        //Load basic data and private data from Server
        basicInfo = ZMasterServer.Instance.Data.DataLoadUserInfoBasic(UID);
        privateInfo = ZMasterServer.Instance.Data.DataLoadUserInfoPrivate(UID);

        //Send back to client
        TargetReceiveBasicInfo(basicInfo, privateInfo);

        //Rename to easy dev
        gameObject.name = basicInfo.nickname;

        //Add user to list user manager
        ZMasterServer.Instance.UserManager.AddUserOnline(this);
    }
    #endregion

    #region CLIENT HANDLE
    //This function run when user load data complete on server and call it
    [TargetRpc]
    public void TargetReceiveBasicInfo(BasicInfo bInfo, PrivateInfo pInfo)
    {
        //Assign data
        basicInfo = bInfo;
        privateInfo = pInfo;

        //NOTE: Do not sync UID to client, if you wanna do something, post on server, ZUser on server has UID itself.
        this.PostEvent(ZEventId.OnSendBasicInfor, basicInfo);
        this.PostEvent(ZEventId.OnSendPrivateInfor, privateInfo);

        //Rename to easy dev
        gameObject.name = basicInfo.nickname;
    }
   
    //This function call when cannot create room, the message inside "message"
    [TargetRpc]
    public void TargetReceiveCannotCreateRoom(string message)
    {

    }
    #endregion

    #region SERVER HANDLE


  
    #endregion

    #region SYSTEM HANDLE
    public void DisconnectTimeout(float time)
    {
        // Time counter for disconnect time out
        var timeOut = gameObject.AddComponent<ZTimeCounter>();

        // Add listener when disconnect time counter is finished
        timeOut.OnCounterCompleted += DisconnectTimeOut;

        //Start count time out
        timeOut.StartCounter(time);
        Debug.Log("ZUser: User " + basicInfo.nickname + " start disconnect time out");
    }

    private void DisconnectTimeOut()
    {
        if (isDisconnected)
        {
            Debug.Log("ZUser: " + basicInfo.nickname + " still disconnect after time out, destroy it! Disconnect state: " + isDisconnected);
            ZMasterServer.Instance.UserManager.disconnectedUsers.Remove(this);
            NetworkServer.DestroyPlayerForConnection(this.connectionToClient);
        }
    }

    [Server]
    public void GeneratePlayerToken()
    {
        Guid g = Guid.NewGuid();
        playerToken = Convert.ToBase64String(g.ToByteArray());
        playerToken = playerToken.Replace("=", "");
        playerToken = playerToken.Replace("+", "");
    }
    #endregion
}
