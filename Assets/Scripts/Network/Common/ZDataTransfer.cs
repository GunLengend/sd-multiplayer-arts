﻿using System.Collections.Generic;
using Mirror;
using UnityEngine;

/// <summary>
/// This handle Command & RPC between GameServer
/// and Master Server via local Port.
/// The Game Server must required DataTransfer
/// GameObject to remote connect if they launch in
/// difference physical Server.
/// </summary>


public class ZDataTransfer : NetworkBehaviour
{
    public string roomID;
    public ZGameRoom.RoomGame roomConnectInfo;
    public ZGameRoom.RoomType roomType;
    public ZGameRoom Room;

    public override void OnStartLocalPlayer()
    {
        base.OnStartLocalPlayer();

        //This call on Game Server
        CmdSyncRoomInfo(roomID, roomConnectInfo, roomType);
    }

    public override void OnStartServer()
    {
        base.OnStartServer();

        //This call on Master Server
        //Clear observers list to make sure this not spawn on player client.
        NetworkServer.RebuildObservers(this.netIdentity, false);
    }

    #region ZDataTransfer GAME SERVER
    //Step 1 : Game server assignment, this function call on Game Server when DataTransfer spawned
    public void AssignMatchInfo(string id, ZGameRoom.RoomGame info, ZGameRoom.RoomType type)
    {
        //Assign to DataTransfer on GameServer
        roomID = id;
        roomConnectInfo = info;
        roomType = type;

        Debug.Log("ZDataTransfer: Assign match info " + roomID + " " + roomConnectInfo.IP + " " + roomConnectInfo.port);
    }

    //Sync all user infor from server to player.
    [TargetRpc]
    public void TargetReceivePlayerInfo(NetworkConnection netCon, string playerToken, ZGameInfo.GameInfo gameInfo, ZUser.BasicInfo basicInfo)
    {
        //TODO:
        Debug.Log("ZDataTransfer: Receive player info and send to Client");
    }

    //Step 3 : Game Server receive blackjackInfo
    [TargetRpc]
    public void TargetReceiveGameInfo(ZGameInfo.GameInfo bjInfo)
    {
        Debug.Log("ZDataTransfer: Receive game Info on GameServer");

        //Post event contain info to BlackHost
        this.PostEvent(ZEventId.OnReceiveBlackjackInfo, bjInfo);
    }

    [TargetRpc]
    public void TargetReceiveRoomClose(NetworkConnection con)
    {
        this.PostEvent(ZEventId.OnGameServerClose);
    }

    [TargetRpc]
    public void TargetCheckPlayerConnection(NetworkConnection con, string playerToken)
    {
       
    }
    #endregion

    #region ZDataTransfer MASTER SERVER
    //Step 2: Master server assignment, this function call when this DataTransfer spawned
    [Command]
    public void CmdSyncRoomInfo(string id, ZGameRoom.RoomGame info, ZGameRoom.RoomType type)
    {
        //Assign to DataTransfer on MasterServer
        roomID = id;
        roomConnectInfo = info;
        roomType = type;

        //Find room relate to this DataTransfer
        switch (type)
        {
            case ZGameRoom.RoomType.Normal:
                Room = ZMatchMaking.Instance.listRoomNormal.Find(a => a.roomID == int.Parse(roomID));
                Room.Transfer = this;
                break;
            case ZGameRoom.RoomType.Tournament:
                Room = ZMatchMaking.Instance.listRoomRanking.Find(a => a.roomID == int.Parse(roomID));
                Room.Transfer = this;
                break;
        }

        if (Room.roomID != -1)
            gameObject.name = "DataTransfer - Room " + Room.roomID.ToString();

        Debug.Log("ZDataTransfer: Find out Room: " + Room.roomID + " and roomID: " + roomID);

        //Collect game info of that room
        ZGameInfo.GameInfo gameInfo = new ZGameInfo.GameInfo();

        Debug.Log("ZDataTransfer: Get game info on MasterServer");

        //Send the blackjack info to GameServer
        TargetReceiveGameInfo(gameInfo);
    }

    [Command]
    public void CmdSyncPlayerInfo(string playerToken)
    {
        //Find the user has same playerToken
        ZUser userRelate = Room.allUser.Find(x => x.playerToken.Equals(playerToken));

        //Get data and send to ZDataTransfer on GameServer
        TargetReceivePlayerInfo(this.connectionToClient, playerToken, userRelate.GameInfo.gameInfo, userRelate.basicInfo);

        Debug.Log("ZDataTransfer: Get and send player info from Master Server to Client");
    }

    [Command]
    public void CmdOnPlayerLeaveRoom(string playerToken)
    {
        //Find the left player in list user.
        var leftPlayer = Room.allUser.Find(x => x.playerToken == playerToken);

        //Then remove the user with the playeToken above.
        if (leftPlayer != null)
        {
            Debug.Log("ZDataTransfer: Request remove player " + leftPlayer.name + " cause it leave room, call Room.OnPlayerLeaveRoom");
            Room.OnPlayerLeaveRoom(leftPlayer);
        }
        //Not find out, that's mean it was destroyed by disconnect time out
        else
        {
            Debug.Log("ZDataTransfer: Request remove player but it doesnt exist, call Room.OnPlayerWasDestroy");
            Room.OnPlayerWasDestroy();
        }
    }

    [Command]
    public void CmdPlayerIsConnected(string playerToken)
    {
        //Find the user.
        var disconnectPlayer = Room.allUser.Find(x => x.playerToken == playerToken);
        if(disconnectPlayer != null)
        {
            disconnectPlayer.isDisconnected = false;
            var timeCounter = disconnectPlayer.GetComponent<ZTimeCounter>();

            Debug.Log("ZDataTransfer: User" + disconnectPlayer.basicInfo.nickname + "stop disconnect time out");

            //Stop disconnect time out
            if (timeCounter != null)
                timeCounter.StopCounter();

            Debug.Log("ZDataTransfer : User " + disconnectPlayer.basicInfo.nickname + " is disconnect but still connected in GameServer, removed disconnected state");
        }
    }

    [Command]
    public void CmdOnLockRoom()
    {
        Room.roomState = ZGameRoom.RoomState.Full;
    }
    #endregion
}
