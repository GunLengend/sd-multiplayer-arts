﻿using System.Collections;
using UnityEngine;
using Mirror;

/// <summary>
/// "Player" is different from "User". This is your "character"
/// using for control in-game playing. For easy understanding, this
/// is your "hero" in game. This only spawned and available on your
/// Device and Game Server, while "User" spawned both on Game Server 
/// and Master Server.
/// </summary>

public class ZPlayer : NetworkBehaviour
{
    [SyncVar] public bool isHostPlayer;
    [SyncVar] public string playerToken;

    public ZGameInfo.GameInfo GameInfo;
    public ZUser.BasicInfo BasicInfo;

    public bool isDisconnected;

    private void Start()
    {
        if (isLocalPlayer)
        {
            ZGame.Instance.Player = this;
            CmdSyncPlayerToken(ZGame.Instance.localPlayerToken);
        }
    }

    #region PLAYER CLIENT
    //Using for sync player info of any player
    [ClientRpc]
    public void RpcReceivePlayerInfo(uint playerReceiveNetId, ZGameInfo.GameInfo gameInfo, ZUser.BasicInfo basicInfo)
    {
        if (netId == playerReceiveNetId)
        {
            GameInfo = gameInfo;
            BasicInfo = basicInfo;
        }

        //Disable loading panel
        Debug.Log("ZPlayer: Data load from GameServer completed!");
    }

    // Using for sync player info of local player
    [TargetRpc]
    public void TargetReceivePlayerInfo(ZGameInfo.GameInfo gameInfo, ZUser.BasicInfo basicInfo)
    {
       GameInfo = gameInfo;
       BasicInfo = basicInfo;
    }

    // Using for sync game info
    [TargetRpc]
    public void TargetReceiveGameInfo(ZGameInfo.GameInfo gameInfo)
    {
       
    }

    [TargetRpc]
    public void TargetWaitingForOtherPlayer()
    {
        this.PostEvent(ZEventId.OnWaitingForOtherPlayer);
    }

    #endregion

    #region PLAYER SERVER  
    [Command]
    public void CmdSyncPlayerToken(string localPlayerToken)
    {
        playerToken = localPlayerToken;

        //Get data from user to this player based on playerToken.
        ZGameServer.Instance.Transfer.CmdSyncPlayerInfo(playerToken);

        Debug.Log("Player: Request get data player info to Master Server");
    }

    // Đăng ký leave room
    [Command]
    public void CmdRegisterLeaveRoom()
    {
       
    }

    [Command]
    public void CmdCancelLeaveRoom()
    {
      
    }

    #endregion

    #region SYSTEM HANDLE
    public void DisconnectTimeout(float time)
    {
        // Time counter for disconnect time out
        var timeOut = gameObject.AddComponent<ZTimeCounter>();

        // Add listener when disconnect time counter is finished
        timeOut.OnCounterCompleted += DisconnectTimeOut;

        //Start count time out
        timeOut.StartCounter(time);
        Debug.Log("ZPlayer: Player " + BasicInfo.nickname + " start disconnect time out");
    }

    private void DisconnectTimeOut()
    {
        if (isDisconnected)
        {
            Debug.Log("ZPlayer: Still disconnect after time out, call ZBlackHost.OnPlayerLeaveRoom");
        }
    }

    public void RequestLeaveRoom()
    {
        
    }
    #endregion
}
