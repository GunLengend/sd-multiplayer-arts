﻿using Mirror;

public struct ReconnectMessage : NetworkMessage
{
    public System.Guid assetId;
    public string syncCondition;
    public uint netId;
}

public struct StringMessage : NetworkMessage
{
    public string stringValue;
}

public struct IntMessage : NetworkMessage
{
    public int intValue;
}

public struct SpawnMessage : NetworkMessage
{
    public string objectName;
    public string objectType;
}

#if ENABLE_MATCH_MAKING_MODE
public struct ObjectAddTypeMessage : NetworkMessage
{
    public enum ObjType
    {
        Player,
        Transfer
    }

    public string strIncluded;
    public ObjType objType;
}
#endif

public struct LoginMessage : NetworkMessage
{
    public string username;
    public string password;
    public string userID;
    public string accessToken;
    public string deviceID;
    public ZLogin.LoginType loginType;
}

public struct LoginCallbackMessage : NetworkMessage
{
    public enum AuthCode
    {
        Sucessfull = 0,
        WrongUserOrPass = 1,
        AccountNotExist = 2,
        AccountLogined = 3,
        UnknowError = 4
    }

    public string UID;
    public AuthCode authCode;
}

public struct RegistrationMessage : NetworkMessage
{
    public string username;
    public string password;
    public string name;
    public string email;
    public string phoneNumber;
    public string address;
    public string gender;
    public string about;
    public string urlAvartar;
    public string idFacebook;
    public string idGoogle;
    public string idDevice;
    public ZLogin.LoginType loginType;
}

public struct RegistrationCallbackMessage : NetworkMessage
{
    public enum RegCode
    {
        Successfull = 0,
        UsernameAlreadyExist = 1,
        EmailAlreadyUsed = 2,
        UnknowError = 3
    }

    public RegCode regCode;
    public string UID;
}
