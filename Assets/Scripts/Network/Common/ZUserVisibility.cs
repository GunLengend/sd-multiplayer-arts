﻿using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZUserVisibility : NetworkVisibility
{
    private ZUser User;

    private void Awake()
    {
        User = GetComponent<ZUser>();
    }

    //This prevent add any connection to list
    public override bool OnCheckObserver(NetworkConnection newObserver)
    {
        return false;
    }

    //This let user only see player in room
    public override void OnRebuildObservers(HashSet<NetworkConnection> observers, bool initialize)
    {
        observers.Clear();

        if (User.isJoinedRoom)
        {
            ZGameRoom Room = GetComponentInParent<ZGameRoom>();

            for (int i = 0; i < Room.allUser.Count; i++)
            {
                observers.Add(Room.allUser[i].connectionToClient);
            }
        }
        else
        {
            observers.Add(connectionToClient);
        }
    }

    public override void OnSetHostVisibility(bool visible)
    {
        base.OnSetHostVisibility(true);
    }
}
