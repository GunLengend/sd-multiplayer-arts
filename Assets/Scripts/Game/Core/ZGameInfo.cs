﻿using UnityEngine;
using Mirror;

public class ZGameInfo : NetworkBehaviour
{
    [System.Serializable]
    public struct GameInfo
    {
        public long gold;
        public long cash;
        public long exp;
        public int gamePlayed;
        public int winCount;
        public int loseCount;
    }

    public GameInfo gameInfo;

    [HideInInspector] public ZUser User;

    #region INITIAL AND REFERENCE
    void Awake()
    {
        //Global reference, it will assign bot Client and Server
        User = GetComponent<ZUser>();
    }

    public override void OnStartServer()
    {
        base.OnStartServer();

        //On start, load game info from data on Server
        gameInfo = ZMasterServer.Instance.Data.DataLoadUserInfoDataGame(User.UID);

        //Then, send back to client
        TargetReceiveGameInfo(this.connectionToClient, gameInfo);
    }

    public override void OnStartClient()
    {
        base.OnStartClient();

        //Event register
    }
    #endregion

    #region CLIENT HANDLE
    //This function used to receive game info on client
    [TargetRpc]
    public void TargetReceiveGameInfo(NetworkConnection conn, ZGameInfo.GameInfo info)
    {
        gameInfo = info;
        this.PostEvent(ZEventId.OnSendGameInfor, gameInfo);
    }
    #endregion

    #region SERVER HANDLE
    
    #endregion
}
