﻿using UnityEngine;
using Mirror;
using System.Collections;
using UnityEngine.SceneManagement;

//Handle in-game event and spawn player
public class ZGame : MonoBehaviour 
{
    [HideInInspector]public ZGlobalAsset Asset;

    public ZUser User;
    public ZPlayer Player;
    public GameObject userPrefabs;
    public MatchInfo matchInfo;

    public string localPlayerToken;
    public string localUserID;

    private ZUser spawnedUser;

    //Singleton pattern initial
    private static ZGame instance;
    public static ZGame Instance { get { return instance; } }

	#region INITIAL AND REFERENCE
    void Awake()
    {
        DontDestroyOnLoad(this);

        if (instance == null) 
            instance = this;
        else
            Destroy(gameObject);
    }
   
	void Start () 
    {
        //Reference
        if(Asset == null)
            Asset = GameObject.FindGameObjectWithTag("GlobalAsset").GetComponent<ZGlobalAsset>();

        //Event handle
		this.RegisterListener(ZEventId.OnUserLoginSuccess, (sender, param) => OnGameSpawnUser((string)param));
        this.RegisterListener(ZEventId.OnGameMatchFound, (sender, param) => OnGameLoadMap((MatchInfo) param));
	}
	#endregion

    #region LOADING GAME/MAP SCENE
    //This function call when player find out match and connect to game server
    void OnGameLoadMap(MatchInfo match)
    {
        //Assign matchInfo
        matchInfo = match;
        localPlayerToken = match.playerToken;

        //Start load game map
        StartCoroutine(LoadGameMap(matchInfo));
    }

    IEnumerator LoadGameMap(MatchInfo matchInfo)
    {
        //Post event OnGameLoadingMap
        this.PostEvent(ZEventId.OnGameLoadingMap);

        //Load sence, if this game contain specific map for room, it must be using matchInfo.mapName
        AsyncOperation async = SceneManager.LoadSceneAsync("Game");

        //Wait to localPlayer spawn on client complete. That's mean already connected
        yield return new WaitUntil(() => Player != null);
        Debug.Log("ZGame: Player spawned and connect to GameServer completed");
    }

    //This function call when player leave game and back to lobby
    public void OnBackToLobby()
    {

        if (ZMatchClient.Instance.isDisconnected)
        {
            ZMatchClient.Instance.Disconnect();
            StartCoroutine(OnReconnectToLobby());
        }
        else
            Player.CmdRegisterLeaveRoom();
    }

    IEnumerator OnReconnectToLobby()
    {
        //Load sence
        var lobby = SceneManager.LoadSceneAsync("MainDev");
        yield return new WaitUntil(() => lobby.isDone);

        Debug.Log("ZGame : Load Client scene complete");

        yield return new WaitUntil(() => ZMasterClient.Instance.isConnected);

        //Then spawn user again.
        OnGameRespawnUser(localUserID);
    }
    #endregion

    #region SPAWN USER/PLAYER
    //This function call when event OnPlayerLoginSuccess
    public void OnGameSpawnUser(string uID)
    {
        //Stored local userUID
        localUserID = uID;

        //Assign player prefabs from asset
        userPrefabs = Asset.userAsset;

        //Register user prefab with spawning system
        NetworkClient.RegisterPrefab(userPrefabs, OnSpawnUser, OnDespawnUser);

        //Set client to be ready
        if (ZMasterClient.Instance != null && !NetworkClient.ready)
        {
            NetworkClient.Ready();
        }

        //Create custom message
        ObjectAddTypeMessage msg = new ObjectAddTypeMessage
        {
            strIncluded = uID,
            objType = ObjectAddTypeMessage.ObjType.Player
        };

        //Send spawn player message to server
        NetworkClient.Send(msg);

        //Post event spawn player success
        this.PostEvent(ZEventId.OnUserSpawnComplete);

#if DEBUG
        Debug.Log("ZGame: Client spawn player complete !");
#endif
    }

    public void OnGameRespawnUser(string uID)
    {
        //Stored local userUID
        localUserID = uID;

        //Assign player prefabs from asset
        userPrefabs = Asset.userAsset;

        //Register user prefab with spawning system
        NetworkClient.RegisterPrefab(userPrefabs, OnSpawnUser, OnDespawnUser);

        //Create custom message
        ObjectAddTypeMessage msg = new ObjectAddTypeMessage
        {
            strIncluded = uID,
            objType = ObjectAddTypeMessage.ObjType.Player
        };

        //Set client to be ready
        if (ZMasterClient.Instance != null && !NetworkClient.ready)
        {
            NetworkClient.Ready();
        }

        //Send spawn player message to server
        NetworkClient.Send(msg);

        //Post event re-spawn player success
        this.PostEvent(ZEventId.OnUserReSpawnComplete);

#if DEBUG
        Debug.Log("ZGame: Client respawn player complete !");
#endif
    }

    //This is spawn handler
    GameObject OnSpawnUser(Vector3 position, System.Guid assetId)
    {
        if (ZMasterClient.Instance != null)
        {
            //Spawn user prefab
            GameObject user = Instantiate(userPrefabs, userPrefabs.transform.position, Quaternion.identity) as GameObject;
            spawnedUser = user.GetComponent<ZUser>();
            spawnedUser.transform.parent = ZMasterClient.Instance.transform;
            return user;
        }
        return null;  
    }

    //This is despawn handler
    void OnDespawnUser(GameObject spawned)
    {
        Destroy(spawned);
    }

    #endregion

    #region GENERIC FUNCTION

    //This function call when player log out
    public void OnGameLogOut()
    {
        StringMessage msg = new StringMessage
        {
            stringValue = localUserID
        };

        NetworkClient.Send(msg);
        this.PostEvent(ZEventId.OnUserLogOut, localUserID);
    }
    #endregion
}